function myFunction() {
    document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://127.0.0.1:8000/contact/import/google";
}
function enable(checkbox)//when clicking a checkbox this function makes that the input get enabled/disabled
{
    var contact_id = checkbox.closest("tr").querySelector('.contact_id')
    contact_id.disabled = !checkbox.checked
}
function toggle_inputs(o) {
        var contact_id = document.getElementsByClassName('contact_id');
        var check = document.getElementsByClassName('check');
        for (var i = contact_id.length, n = 0; n < i; n++) {
            contact_id[n].disabled = !contact_id[n].disabled;
        }
        var boxes = document.getElementsByTagName("input");
        for (var x = 0; x < boxes.length; x++) {
        var obj = boxes[x];
        if (obj.type == "checkbox") {
            if (obj.name != "check")
                obj.checked = o.checked;
        }
    }
}