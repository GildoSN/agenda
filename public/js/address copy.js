
function auto_preenche(elemento)
{
    function limpa_formulário_cep() {
        for ($i=0; $i < count($data['cep']); $i++) { 
            //Limpa valores do formulário de cep.
            document.getElementById('rua').value=("");
            document.getElementById('bairro').value=("");
            document.getElementById('localidade').value=("");
            document.getElementById('uf').value=("");
            document.getElementById('ibge').value=("");
            document.getElementById('unidade').value=("");
            document.getElementById('complemento').value=("");
        }
            
    }

    function meu_callback(conteudo,elemento) {

        if (!("erro" in conteudo)) {
            //Atualiza os campos com os valores.
            elemento.closest(".address_container").querySelector('[name="rua[]"]').value=(conteudo.logradouro);
            document.getElementById('bairro').value=(conteudo.bairro);
            document.getElementById('localidade').value=(conteudo.localidade);
            document.getElementById('uf').value=(conteudo.uf);
            document.getElementById('ibge').value=(conteudo.ibge);
            document.getElementById('unidade').value=(conteudo.unidade);
            document.getElementById('complemento').value=(conteudo.complemento);
        } //end if.
        else {
            //CEP não Encontrado.
            limpa_formulário_cep();
            alert("CEP não encontrado.");
        }
    }
        
    function pesquisacep(elemento) {

        //Nova variável "cep" somente com dígitos.
        var cep = elemento.value.replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {



                //Preenche os campos com "..." enquanto consulta webservice.

                elemento.closest(".address_container").querySelector('[name="rua[]"]').value="...";

                elemento.closest(".address_container").querySelector('[name="bairro[]"]').value="...";

                elemento.closest(".address_container").querySelector('[name="localidade[]"]').value="...";

                elemento.closest(".address_container").querySelector('[name="uf[]"]').value="...";

                elemento.closest(".address_container").querySelector('[name="ibge[]"]').value="...";

                elemento.closest(".address_container").querySelector('[name="unidade[]"]').value="...";


                elemento.closest(".address_container").querySelector('[name="complemento[]"]').value="...";

                //Cria um elemento javascript.
                var script = document.createElement('script');

                //Sincroniza com o callback.
                script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

                //Insere script no documento e carrega o conteúdo.
                document.body.appendChild(script);

            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    };
}