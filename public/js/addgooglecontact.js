

var maskBehavior = function (val) {
return val.replace(/\D/g, '').length === 11 ? '(00) 00000-000000' : '(00)(00) 0000-00009';
},
options = {onKeyPress: function(val, e, field, options) {
        field.mask(maskBehavior.apply({}, arguments), options);
    }
};

$('.phone').mask(maskBehavior, options);


function enable(checkbox)//when clicking a checkbox this function makes that the inputs get enabled/disabled
{
    // console.log(checkbox);
    // console.log(checkbox.closest(".card-pf-body").querySelector(".name"));
    // console.log(checkbox.closest("textarea"));

    var name = checkbox.closest(".card-pf-body").querySelector('.name')
    var phone = checkbox.closest(".card-pf-body").querySelector('.phone')
    var email = checkbox.closest(".card-pf-body").querySelector('.email')
    name.disabled = !checkbox.checked
    phone.disabled = !checkbox.checked
    email.disabled = !checkbox.checked

}

$("#checkAll").click(function () {
    // $(".check").click();
    $(".check").prop('checked', $(this).prop('checked'));//checks all the checkboxes

    if($('.check').prop('checked')){
        $("#myForm .name").prop("disabled", false);
        $("#myForm .phone").prop("disabled", false);
        $("#myForm .email").prop("disabled", false);
    }
    else{
        $("#myForm .name").prop("disabled", true);
        $("#myForm .phone").prop("disabled", true);
        $("#myForm .email").prop("disabled", true);
    }
});
$("#checkAll_down").click(function () {
    // $(".check").click();
    $(".check").prop('checked', $(this).prop('checked'));//checks all the checkboxes

    if($('.check').prop('checked')){
        $("#myForm .name").prop("disabled", false);
        $("#myForm .phone").prop("disabled", false);
        $("#myForm .email").prop("disabled", false);
    }
    else{
        $("#myForm .name").prop("disabled", true);
        $("#myForm .phone").prop("disabled", true);
        $("#myForm .email").prop("disabled", true);
    }
});

/*Scroll to top when arrow up clicked BEGIN*/
$(window).scroll(function() {
    var height = $(window).scrollTop();
    if (height > 100) {
        $('#back2Top').fadeIn();
    } else {
        $('#back2Top').fadeOut();
    }
});
$(document).ready(function() {
    $("#back2Top").click(function(event) {
        event.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

});
 /*Scroll to top when arrow up clicked END*/