
var maskBehavior = function (val) {
return val.replace(/\D/g, '').length === 11 ? '(00) 00000-000000' : '(00)(00) 0000-00009';
},
options = {onKeyPress: function(val, e, field, options) {
        field.mask(maskBehavior.apply({}, arguments), options);
    }
};

$('.phone').mask(maskBehavior, options);


$(document).ready(function(){
    $('.cpf').mask('000.000.000-00');
    $('.cep').mask('00000-000');

    //event para ver $this value . length
    // $('.phone').mask('(55)(99)9999-9990000');


    $('.rg').mask('00.000.000');
    $('.uf').mask('SS');
});


$("#myForm").submit(function() {
    $('.cpf').unmask();
    $('.cep').unmask();
    $('.phone').unmask();
    $('.rg').unmask();
    $('.uf').unmask();

});
$('.remove').click(function () {
    $(this).parents("tr").remove();
});

$('#multiple-checkboxes').multiselect({
    includeSelectAllOption: true,
});

$(document).ready(function(){
    document.getElementById("cria-categoria").addEventListener("click", function(e){
    // $('#cria-categoria').on('click', function(e){
        e.preventDefault();

        $.ajax({
            type:'POST',
            url:'/categories',
            data: $('#cria_categoria').serialize(),
            success: function (response) {
                // console.log(response)
                $('#exampleModalCenter').modal('hide')
                alert('Categoria criada, selecione ela no campo de categorias');

                $("#refresh").load(location.href + " #refresh>*" ,function(){
                    $('#multiple-checkboxes').multiselect({
                        includeSelectAllOption: true,
                    });
                });
                // $("#refresh").load("newcontact.blade.html");

            },
            error: function(error){
                // console.log(error)
                alert('data not saved');

            }
        });
    });
});

function addToDelete(deleteid){
    var toDeletefield = document.getElementById("idsToDelete");
    var addToDelete = ((toDeletefield.value==="") ?  deleteid : (","+deleteid))
    toDeletefield.value +=  addToDelete;
}

var i=0;



//clona div address_container e limpa campos dela
var i=0;
var c = 1;
function clone()
{
    var elmnt = document.getElementsByClassName("address_container")[0];
    elmnt.style.marginBottom = "1%"
    var addressesDiv = document.getElementById('address');
    var cloneElement = elmnt.cloneNode(true);
    $a = cloneElement.getElementsByTagName('input')
    for (let index = 0; index < $a.length; index++) {
        const element = $a[index];
        element.setAttribute('name', 'newAddresses['+ c +']['+element.id+']');
        element.removeAttribute('id');
    }
    cloneElement.getElementsByTagName('input').innerHTML=""
    cloneElement.style.display = 'block';
    const button = document.createElement('button');
    button.className = "i btn btn-danger"
    console.log(button);
    button.type= "button"
    button.innerHTML = "Deletar"
    cloneElement.appendChild(button);
    addressesDiv.appendChild(cloneElement);
    $('.address_container').last().find('input:text').val('');
    c++
    console.log(c);
    return c;
}

$(document).on('mouseenter', '.divbutton', function () {
    $(this).find(".i").show();
}).on('mouseleave', '.divbutton', function () {
    $(this).find(".i").hide();
}).on('click', '.i', function() {
    $(this).parent().remove();
});