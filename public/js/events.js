$('.edit').on('show.bs.modal',function(event){ //auto completa os inputs que estão dentro da modal 
    // console.log('oalalla');
    var button = $(event.relatedTarget)
    var categorybutton = button.data('event_title')
    var start_datebutton = button.data('start_date')
    var end_datebutton = button.data('end_date')
    var event_id = button.data('event_id')

    var modal = $(this)

    modal.find('.modal-body #categoryy').val(categorybutton)
    modal.find('.modal-body #start_date').val(start_datebutton)
    modal.find('.modal-body #end_date').val(end_datebutton)

    modal.find('.modal-body #cat_id').val(event_id)
})

moment.updateLocale('en', {
    months : [
        "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
    ],
    weekdays : [
        "Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sabado"
    ],
    weekdaysShort : ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
    weekdaysMin : ["D", "S", "T", "Q", "Q", "S", "S"]
});

$(function () {
    $('#example1').datetimepicker({
        format:"D-M-Y HH:mm"
    });
});
$(function () {
    $('#example2').datetimepicker({
        format:"D-M-Y HH:mm"
    });
});   

$(function () {
    $('.datetimepickergb ').datetimepicker({
      format:"D-M-Y HH:mm"
    });
  });