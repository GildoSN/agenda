
var elemento;

    function limpa_formulário_cep() {
        try{

            for ($i=0; $i < count($data['cep']); $i++) {
                //Limpa valores do formulário de cep.
                elemento.closest(".address_container").querySelector('[name*="logradouro"]').value=("");
                elemento.closest(".address_container").querySelector('[name*="bairro"]').value=("");
                elemento.closest(".address_container").querySelector('[name*="localidade"]').value=("");
                elemento.closest(".address_container").querySelector('[name*="uf"]').value=("");
                elemento.closest(".address_container").querySelector('[name*="ibge"]').value=("");
                elemento.closest(".address_container").querySelector('[name*="unidade"]').value=("");
                elemento.closest(".address_container").querySelector('[name*="complemento"]').value=("");
            }
        }
        catch(e){
        }

    }

    function meu_callback(conteudo) {

        if (!("erro" in conteudo)) {
            //Atualiza os campos com os valores.
            elemento.closest(".address_container").querySelector('[name*="logradouro"]').value=(conteudo.logradouro);
            elemento.closest(".address_container").querySelector('[name*="bairro"]').value=(conteudo.bairro);
            elemento.closest(".address_container").querySelector('[name*="localidade"]').value=(conteudo.localidade);
            elemento.closest(".address_container").querySelector('[name*="uf"]').value=(conteudo.uf);
            elemento.closest(".address_container").querySelector('[name*="ibge"]').value=(conteudo.ibge);
            elemento.closest(".address_container").querySelector('[name*="unidade"]').value=(conteudo.unidade);
            elemento.closest(".address_container").querySelector('[name*="complemento"]').value=(conteudo.complemento);
        } //end if.
        else {
            //CEP não Encontrado.
            limpa_formulário_cep();
            alert("CEP não encontrado.");
        }
    }

    function pesquisacep(elementoAlvo) {
        elemento = elementoAlvo;


        //Nova variável "cep" somente com dígitos.
        var cep = elementoAlvo.value.replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {



                //Preenche os campos com "..." enquanto consulta webservice.
                console.log(elementoAlvo.closest(".address_container"));

                elementoAlvo.closest(".address_container").querySelector('[name*="logradouro"]').value="...";


                elementoAlvo.closest(".address_container").querySelector('[name*="bairro"]').value="...";

                elementoAlvo.closest(".address_container").querySelector('[name*="localidade"]').value="...";

                elementoAlvo.closest(".address_container").querySelector('[name*="uf"]').value="...";

                elementoAlvo.closest(".address_container").querySelector('[name*="ibge"]').value="...";

                elementoAlvo.closest(".address_container").querySelector('[name*="unidade"]').value="...";


                elementoAlvo.closest(".address_container").querySelector('[name*="complemento"]').value="...";

                //Cria um elemento javascript.
                var script = document.createElement('script');

                //Sincroniza com o callback.
                script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

                //Insere script no documento e carrega o conteúdo.
                document.body.appendChild(script);

            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    };
