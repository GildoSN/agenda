<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\User;
use App\Models\Contact;
use Faker\Generator as Faker;

$factory->define(Contact::class, function (Faker $faker) {
    return [
        'name'     => $faker->name,
        'cpf'      => $faker->cpf,
        'rg'       => $faker->rg,
        'email'    => $faker->unique()->safeEmail,
        'status'   => 1, // password
        'users_id' => factory(User::class)->create()->id,
        'favorite' => 0,
    ];
});
