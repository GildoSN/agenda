<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Address;
use App\Models\Contact;
use Faker\Generator as Faker;

$factory->define(Address::class, function (Faker $faker) {
    return [
        'cep'         => $faker->postcode,
        'logradouro'  => $faker->streetName,
        'complemento' => $faker->streetSuffix,
        'bairro'      => $faker->citySuffix,
        'localidade'  => $faker->regionAbbr,
        'uf'          => $faker->stateAbbr,
        'unidade'     => $faker->randomNumber,
        'ibge'        => $faker->randomNumber(3),
        'contact_id'  => factory(Contact::class)->create()->id,
    ];
});
