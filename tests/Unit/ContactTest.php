<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use App\Models\Contact;
use App\Models\Usuario;
use App\Services\ContactService;
use App\Services\Responses\ServiceResponse;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ContactTest extends TestCase
{
    use DatabaseTransactions;

    private $contactService;

    public function setUp(): void
    {
        parent::setUp();

        $this->contactService = app(ContactService::class);
    }

    public function testErrorWhenFindingAnotherUsersContact()
    {
        $user = factory(User::class)->create();
        $contact = factory(Contact::class)->create();

        $contact = $this->contactService->find($user->id, $contact->id);

        $this->assertInstanceOf(ServiceResponse::class, $contact);
        $this->assertFalse($contact->success);
        $this->assertNull($contact->data);
    }

    public function testFindingUsersContact()
    {
        $contact = factory(Contact::class)->create();

        $contact = $this->contactService->find($contact->users_id, $contact->id);

        $this->assertInstanceOf(ServiceResponse::class, $contact);
        $this->assertTrue($contact->success);
        $this->assertNotNull($contact->data);
    }

    public function testDeleteAnotherUsersContact()
    {
        $user = factory(User::class)->create();
        $contact = factory(Contact::class)->create();

        $contact = $this->contactService->delete($user->id, $contact->id);

        $this->assertInstanceOf(ServiceResponse::class, $contact);
        $this->assertFalse($contact->success);
        $this->assertNull($contact->data);
    }

    public function testDeleteContact()
    {
        $contact = factory(Contact::class)->create();

        $contact = $this->contactService->delete($contact->users_id, $contact->id);

        $this->assertInstanceOf(ServiceResponse::class, $contact);
        $this->assertTrue($contact->success);
        $this->assertNotNull($contact->data);
    }
}
