var elixir = require('laravel-elixir');

elixir(function(mix) {
    mix.sass('app.scss').browserify('app.js');
    mix.less('/node_modules/selectize/src/less/selectize.bootstrap3.less', 'public/css');
});