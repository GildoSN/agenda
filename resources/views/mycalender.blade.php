@extends('navbar')
@section('content')


    <div class="container">

        <div class="row">
            @if (count($errors) > 0 )
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>    
                    @endforeach
                </ul>
            </div>
            @endif
            
            <div class="text-center">
                <h1>Excel Contacts</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
            
                {{-- @if($contacts->count() > 9)
                    <div class="col-lg-3  pull-right">
                        <form action="/excel/search" method="GET">
                        <p>Procure um contato:</p>
                        <div class="input-group">
                            <input type="search" placeholder="nome do contato:" name="search" class="form-control">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary" > 
                                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                                </button>
                            </span>
                        </div>
                        </form>
                    </div>
                @endif --}}
                
                <button type="button" style="margin-top:3%;" class="btn btn-info" onclick="myFunction()">Importar contatos da google</button>
                {!! Form::open(['route' => 'export_excel.excel', 'id'=>'myForm']) !!}
                {{ csrf_field() }}
            </div>
        </div>

        <div class="row">
            <div class="table-responsive">
                <table class="table table-striped " >
                    @if($contacts->count() > 0)
                        <thead>
                            <tr>
                            <th class="text-center">Nome</th>
                            <th class="text-center">Email</th>
                            <th style="text-align:center;"><input type="checkbox" style="vertical-align: middle; " class="check" id="checkAll"></th>

                            </tr>
                        </thead>
                    @endif
                    <tbody>
                        @forelse ($contacts as $contact)
                            <tr class="text-center">

                                <input name="contactid[]" disabled class="contact_id" id="contact-id" value="{{$contact->id}}" type="hidden"> 
                                <td style="vertical-align: middle;"><input id="inp" type="text" class="name" name="name[]" disabled  value="{{$contact->name }}"></td>
                                <td style="vertical-align: middle;"><input id="inp" class="email" type="text" name="email[]" disabled  value="{{$contact->email }}"></td>
                                <td><input type="checkbox" id="checkb" name="contact[]" onchange="enable(this)"  class="check"></td>
                        
                            </tr>
                        @empty
                            <p>nenhum contato registrado :)</p>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">    
            {{-- <span id="pagi" class="line">{{ $contacts->links() }}</span> --}}

            <a href="{{ route('export_excel.excel') }}">
                <button type="submit" class="line btn btn-success pull-right">Exportar</button>
            </a>

            <button class="line btn btn-danger pull-right" type="button" data-toggle="collapse"
             data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Requisitos para Exportar contatos  
                <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span> 
            </button>
            <div class="collapse pull-right" id="collapseExample">
                <div class="card card-body">
                        <p style="color:red">apenas contatos cujos dados estão completos podem ser enviados para um documento Excel</p>
                </div>
            </div>
        </div> 
    </div>
    {!! Form::close() !!}

    <script>

        function myFunction() {      
            document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://127.0.0.1:8000/contact/import/google";
        }

        function enable(checkbox)//when clicking a checkbox this function makes that the inputs get enabled/disabled
        {
            // console.log(checkbox);
            var name = checkbox.closest("tr").querySelector('.name')
            var email = checkbox.closest("tr").querySelector('.email')
            var contact_id = checkbox.closest("tr").querySelector('.contact_id')
            // console.log(checkbox.closest("tr").querySelector('.name'));
            name.disabled = !checkbox.checked
            email.disabled = !checkbox.checked
            contact_id.disabled = !checkbox.checked

        }

        // $("#checkAll").click(function () {
        //     // $(".check").click();
        //     if($('#myForm .name').prop('disabled')){
        //         $("#myForm .name").prop("disabled", false);
        //         $("#myForm .email").prop("disabled", false);
        //         $("#myForm .contact_id").prop("disabled", false);
        //     }
        //     else{
        //         $("#myForm .name").prop("disabled", true);
        //         $("#myForm .email").prop("disabled", true);
        //         $("#myForm .contact_id").prop("disabled", true);
        //     }
        //     $(".check").prop('checked', $(this).prop('checked'));//checks all the checkboxes
        // });

        $("#checkAll").click(function () {

            console.log($('#myForm .name'));
            // $(".check").click();

            if($('#myForm .name').prop('disabled')){
                $("#myForm .name").prop("disabled", false);
                $("#myForm .contact_id").prop("disabled", false);
                $("#myForm .email").prop("disabled", false);
            }
            else{
                $("#myForm .name").prop("disabled", true);
                $("#myForm .contact_id").prop("disabled", true);
                $("#myForm .email").prop("disabled", true);
            }
            $(".check").prop('checked', $(this).prop('checked'));//checks all the checkboxes
        });
    </script>

    <style>
    
        .line{
            display: inline-block;

        }
        .pagination{
            margin:0;
        }
      
        input[type=text] {
            border: none;
            background-color: transparent;
        }
    </style>
    
@stop