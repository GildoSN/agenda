
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>registrar</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"  href="{!!  asset('/css/login-regis.css') !!}" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
    <body>
        <div class="login-form">

            <form action="/register/user" method="POST">
                @include('flash::message')
                <h2 title="registrar-se" class="text-center">Registrar-se</h2>

                {{ csrf_field() }}
                @if (count($errors) > 0 )
                <div class="alert alert-danger">

                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                @if(session()->has('message'))
                    <div class="alert alert-info">{{ session('message') }}</div>
                @endif

                <div class="form-group">

                    <input title="campo para inserir nome de usuário" name="user_name" type="text" class="form-control" placeholder="Nome de usuário" required="required">

                </div>

                <div class="form-group">

                    <input title="campo para inserir email" name="email" type="email" class="form-control" placeholder="endereço de email" required="required">

                </div>

                <div class="form-group">

                    <input title="campo para inserir a sua senha" name="password" type="password" class="form-control" placeholder="senha" required="required">

                </div>

                <div class="form-group">

                    <input title="campo para confirmar a sua senha" name="passwordconfirm" type="password" class="form-control" placeholder="confirme sua senha" required="required">

                </div>

                <div class="form-group">

                    <button title="botão para registrar-se" type="submit" class="btn btn-primary btn-block">Registrar-se</button>

                </div>

                <div>
                    <p class="ou" title="ou"><span>OU</span></p>
                </div>

                <div class="form-group">

                    <a href="/" id="reg"><button title="botão para logar-se" type="button" class="btn btn-primary btn-block">Logar-se</button></a>

                </div>

            </form>
        </div>
    </body>
</html>