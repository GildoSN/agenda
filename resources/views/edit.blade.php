@extends('navbar')
@section('content')
    <script>


        $(document).ready(function(){
             var i=1;
             $('#add').click(function(){
                  i++;
                  $('#dynamic_field').append('<tr id="row'+i+'"><td><input type="text" pattern=".{15,19}" style="margin-bottom: 3px;" class="phone form-control" id="phone" name="phone[]" required="required" placeholder="Telefone" class="form-control" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
                //   $('.phone').mask('(99)9999-99900','(99)(99)9999-9999');
                var maskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-000000' : '(00)(00) 0000-00009';
                },
                options = {onKeyPress: function(val, e, field, options) {
                        field.mask(maskBehavior.apply({}, arguments), options);
                    }
                };

                $('.phone').mask(maskBehavior, options);
             });
             $(document).on('click', '.btn_remove', function(){
                  var button_id = $(this).attr("id");
                  $('#row'+button_id+'').remove();
             });
        });
    </script>
</head>
    <div class="container teste1">
        <h1>Edição de contato</h1>

        {!! Form::open(['action' => ['ContactController@update', $contact->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data','id'=>'myForm']) !!}

        {{-- {!! Form::open(['route' => ['contacts.update', $contact->id]]) !!} --}}
        {{ csrf_field() }}
        @if (count($errors) > 0 )
        {{-- {{ dd($errors->first('phone.*')) }} --}}
            <div class="alert alert-danger">
                <ul>
                    @if($errors->has('phone.*'))
                        <li>{{ $errors->first('phone.*') }}</li>
                    @else
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    @endif
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-4">
                <label class="gildo" for="name">Nome</label>
                <div class="input">
                    <input type="text" value="{{ $contact->name }}" class="form-control" id="name" name="name" placeholder="nome do contato" required="required">
                </div><!-- /input -->
            </div><!-- /.col-lg-6 -->

            <div class="col-lg-4">
                <label class="gildo">Telefone: </label><button type="button" name="add" id="add" class="btn btn-info btn-xs" style="margin:2%;">adicionar outro telefone</button>
                <div class="form-group">
                    <div class="table-responsive">
                        <table id="dynamic_field" style="width: 354px; ">
                            <tr>
                                <input class="hidden" id="idsToDelete" type="string" name="deletePhones" value="">
                                @if(empty($phones))
                                    <td>
                                        <input type="text" pattern=".{15,19}" style="margin-bottom: 3px;" id="phone" name="phone[]" placeholder="Telefone" class="phone form-control" required="required"/>
                                    </td>
                                @endif
                                @foreach($phones as $phone)
                                    <tr>
                                        <td>
                                            <input type="text"  value="{{ $phone->phone }}" pattern=".{15,19}" style="margin-bottom: 3px;" id="phone" name="phone[{{ $phone->id }}]" placeholder="Telefone" class="phone form-control" required="required"/>
                                        </td>
                                        <td>
                                            <button type="button" @if ($loop->first) class="hidden" @endif onclick="addToDelete(this.value)" value="{{$phone->id}}" class="remove btn btn-danger">X</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tr>
                        </table>
                    </div>
                </div>
            </div><!-- /.col-lg-6 -->
            <!-- Default checked -->

            <div id="refresh" class="col-lg-4">

                <label class="gildo" for="categoria">Categoria</label>
                {{-- {!! Form::Label('category','categoria:') !!} --}}
                <div class="btn-group-lg" >

                    {!! Form::select('category[]',
                    $category
                    , $categories, ['class' =>'categorias form-control','id'=>'multiple-checkboxes','multiple'=>'multiple']) !!}

                    <span class="btn-group" >
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModalCenter">
                            nova categoria
                        </button>
                    </span>

                </div><!-- /input-group -->

            </div><!-- /.col-lg-6 -->
        </div>

        <div class="row">
            <div class="col-lg-4">
                <label class="gildo" for="cpf">CPF</label>
                <div class="input">
                    <input type="text" value="{{ $contact->cpf }}"  class="cpf form-control" id="cpf" name="cpf" placeholder="CPF">
                </div><!-- /input -->
            </div><!-- /.col-lg-6 -->

            <div class="col-lg-4">
                <label class="gildo" for="rg">RG</label>
                <div class="input">
                    <input type="text" value="{{ $contact->rg }}"  class="rg form-control" id="rg" name="rg" placeholder="RG">
                </div><!-- /input -->
            </div><!-- /.col-lg-6 -->


            <div class="col-lg-4">
                <label class="gildo" for="email">Email</label>
                <input name="email" value="{{ $contact->email  }}" type="email" class="form-control" id="email" placeholder="Endereço de email">
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <H2 id="endere">Endereço</H2>
                <div class="col-md-12 text-center button-margin">
                    <button style="margin-bottom:20px;" type="button" onclick="clone()" class="btn btn-info">
                        Adicionar Endereços
                    </button>
                </div>
                <div id="address">
                    @foreach($contact->addresses as $address)
                        <div @if ($loop->first) style="margin-bottom:2%;"@endif class="address_container col-md-12" >
                            <div class="row">
                                <div class="col-xs-6 col-md-4">
                                    <label>Cep:
                                    <input name="addresses[{{ $address->id }}][cep]" pattern=".{9,9}" value="{{ $address->cep }}"  class="cep form-control"  type="text" id="cep" value="" size="100" maxlength="9" onblur="pesquisacep(this);" placeholder="CEP" /></label>
                                </div>

                                <div class="col-xs-6 col-md-4">
                                    <label>Rua:
                                    <input name="addresses[{{ $address->id }}][logradouro]" class="form-control" value="{{ $address->logradouro }}"  type="text" id="logradouro" size="60" placeholder="Rua"/></label>
                                </div>
                                <div class="col-xs-6 col-md-4">
                                    <label>Complemento:
                                    <input name="addresses[{{ $address->id }}][complemento]" required="required" type="text" class="form-control" value="{{ $address->complemento }}"  id="complemento" placeholder="complemento" size="60">
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-md-4">
                                    <label>Bairro:
                                    <input name="addresses[{{ $address->id }}][bairro]" class="form-control" value="{{ $address->bairro }}"  type="text" id="bairro" size="100" placeholder="Bairro"/>
                                </div>
                                <div class="col-xs-6 col-md-4">
                                    <label>Cidade:
                                    <input name="addresses[{{ $address->id }}][localidade]" class="form-control" value="{{ $address->localidade }}"  type="text" id="localidade" size="40" placeholder="Cidade"/>
                                </label>
                                </div>
                                <div class="col-xs-6 col-md-4">
                                    <label>Estado:
                                    <input name="addresses[{{ $address->id }}][uf]" class="uf form-control" value="{{ $address->uf }}"  type="text" id="uf" size="2" placeholder="UF"/></label>
                                    <label>IBGE:
                                    <input name="addresses[{{ $address->id }}][ibge]" class="form-control" value="{{ $address->ibge }}"  type="text" id="ibge" size="8" placeholder="IBGE"/></label>
                                    <label>Unidade:
                                    <input name="addresses[{{ $address->id }}][unidade]" type="text" class="form-control" value="{{ $address->unidade }}"  id="unidade" size="8" placeholder="unidade"></label>
                                </div>
                            </div>
                            <button type="button" @if ($loop->first) class="hidden" @endif  id="i" class="btn btn-danger" onclick="document.body.removeChild(this.parentNode)">
                                Deletar
                            </button>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div style="text-align:center;">
            {!! Form::submit('Editar Contato', ['class' => 'btn btn-primary']) !!}
        </div>
    </div>

    {!! Form::close() !!}


    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalCenterTitle">Adicione uma nova categoria</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="cria_categoria">
                            {{ csrf_field() }}
                        <div class="form-row" >
                            <div class="form-group" style="margin-bottom: -15px;">
                                <label for="category">
                                    Nova categoria
                                </label>
                                <input type="text" name="category" class="form-control" id="category" placeholder="categoria">
                            </div>
                        </div>
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sair</button>
                        <button id="cria-categoria" class=" btn btn-primary" type="button">
                            Criar categoria
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/js/edit.js"></script>
    <script type="text/javascript" src="/js/address.js"></script>

@stop