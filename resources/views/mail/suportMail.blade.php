@component('mail::message')
#Email de Suporte
<p>
    o usuário {{ $params['email'] }}
</p>
<p>
    enviou um email com o assunto " {{ $params['assunto'] }} "
</p>
<p>
    com a seguinte mensagem " {{ $params['texto'] }} "
</p>

{{ config('app.name') }}
@endcomponent
