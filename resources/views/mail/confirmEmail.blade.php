@component('mail::message')
# Confirme seu email para poder acessar sua agenda

@component('mail::button', ['url' => $confirmUrl])
Confirmar Email
@endcomponent
{{ config('app.name') }}
@endcomponent