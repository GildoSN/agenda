@extends('navbar')
@section('content')
<head>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/patternfly/3.24.0/css/patternfly-additions.min.css">
</head>
<body class="cards-pf">
    {!! Form::open(['route' => 'addGoogleContacts.store', 'id'=>'myForm']) !!}
    {{ csrf_field() }}
        @if (count($errors) > 0 )
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>    
                    @endforeach
                </ul>
            </div>
        @endif
        @csrf
        <div class="col-md-12">
            <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                <h1>Escolha o(s) contato(s)</h1>
                <p style="display:none" id="todos_up">Escolher todos <input type="checkbox" class="check" id="checkAll">  
            </div>

            <div class="addbutton col-xs-12 col-sm-4 col-md-6 col-lg-6">
                <button type="submit" id="add" class="botao_import btn btn-info">Importar contatos selecionados</button>
                <p style="display:none" id="todos_down">Escolher todos <input type="checkbox" class="check" id="checkAll_down">  

            </div>
        </div>
        <div class="container-fluid container-cards-pf" id="card-container">
            <div   class="row row-cards-pf">
                @forelse ($contact as $contact)
                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
                        <div class="card-pf card-pf-view card-pf-view-select card-pf-view-multi-select">
                            <div  class="card-pf-body">
                                
                                <p class="card-pf-info text-center">
                                    <input  type="checkbox" id="checkb" name="contact[]" onchange="enable(this)"  class="pull-left check">
                                    <strong>Nome:</strong> 
                                </p>

                                <p><textarea maxlength="35" name="name[]" id="inp" cols="20" disabled class="name" rows="2">{{$contact['name'] }}</textarea>
                                </p>
                                <p  class="card-pf-info text-center"><strong>Email:</strong>
                                    <p><textarea name="email[]" maxlength="35" id="inp" cols="20" disabled class="email" rows="2">{{$contact['email'] }}</textarea>
                                    </p>
                                <p class="card-pf-info text-center"><strong>Telefone:</strong>
                                    <input  id="inp" class="phone" type="text" required name="phone[]" disabled  value="{{$contact['phone'] }}">
                                </p>
                            </div>
                        </div>
                    </div>
                @empty
                    <p>Sua conta não tem nenhum contato :/</p>    

                @endforelse
            </div>
        </div>
        <a id="back2Top" title="Back to top" href="#">&#10148;</a>

    {!! Form::close() !!}

<script type="text/javascript" src="/js/addgooglecontact.js"></script>
<link rel="stylesheet" type="text/css"  href="{!!  asset('/css/addGoogleContact.css') !!}" type="text/css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/patternfly/3.24.0/js/patternfly.min.js"></script>


<script>
function funcaowid(x) {
  if (x.matches) { // If media query matches
    document.getElementById("todos_up").style.display="none"
    document.getElementById("todos_down").style.display="block"

  } else {
    document.getElementById("todos_down").style.display="none";
    document.getElementById("todos_up").style.display="block"


  }
}

var x = window.matchMedia("(max-width: 768px)")
console.log(x);
funcaowid(x) // Call listener function at run time
x.addListener(funcaowid) // Attach listener function on state changes
</script>

@stop