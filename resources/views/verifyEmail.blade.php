
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>logar</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"  href="{!!  asset('/css/login-regis.css') !!}" type="text/css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
    <body>
        <div class="login-form">
            @include('flash::message')
            <form action="/verificanew" method="POST">
                @if (count($errors) > 0 )
                    <div class="alert alert-danger">

                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (session('success') )
                    <div class="alert alert-success">

                        <ul>
                            <li>{{ session('success') }}</li>
                        </ul>
                    </div>
                @endif
                <h2 title="título do Login" class="text-center">Verifique Seu Email</h2>

                {{ csrf_field() }}


                @if(session()->has('message'))
                    <div class="alert alert-info">{{ session('message') }}</div>
                @endif


                @if(session()->has('error'))
                    <div class="alert alert-danger">{{ session('error') }}</div>
                @endif

                <div class="form-group">

                    <input title="campo para inserir seu email" name="email" type="email" class="form-control" placeholder="Endereço de email" required="required">

                </div>

                <div class="form-group">

                    <button title="botão para logar-se" type="submit" class="btn btn-primary btn-block">Enviar</button>

                </div>
            </form>

        </div>
    </body>
</html>
