
   @extends('navbar')
   @section('content')
   
   <head>
   
       <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
   
       <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
   
       <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
   
       <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>

       <script src="https://cdn.jsdelivr.net/momentjs/2.14.1/moment.min.js"></script>
        
       <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
        
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">


   </head>
   
   <style>
    .navbar{
        border-radius: 0;
      }
   </style>
   <body>
        
   <div class="container">

   
       <div class="panel panel-primary">
   
           <div class="panel-heading">
   
               Meu Calendario 
   
           </div>
           <span class="btn-group" >
                <button type="button" style="margin-top:5%;margin-left:10%;" class="btn btn-info" data-toggle="modal" data-target="#event_modal">
                    adicionar evento
                </button>
           </span>

           <span class="btn-group" >
                <a href="/dashboard/events"><button type="button" style="margin-top:5%;margin-left:15%;" class="btn btn-warning">
                    editar eventos
                </button>
            </a>
           </span>
           
           <div id="calendar" class="panel-body" >
   
               {!! $calendar->calendar() !!}
   
               {!! $calendar->script() !!}
   
           </div>
   
   </div>

   </body>

   <div class="event modal" tabindex="-1" id="event_modal" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title">Adicione seu evento abaixo</h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="{{ route('dashboard.store')}}" method="POST" >
                {{ method_field('post')}}
                {{ csrf_field() }}
                <div class="modal-body">
                    <label for="event_title">título do evento</label>
                    <input type="text" id="event_title" name="title" placeholder="titulo do evento" class="evento form-control" required="required"/>
                   
                    <div class="form-group">

                        <label for="example1">inicio do evento</label>
                        <div class='input-group date' id='example1' >
                            <input type='text' id="datetimepicker4" name="start_date" class="datetimepickergb  form-control" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="example2">fim do evento</label>
                        <div class='input-group date' id='example2'>
                            <input type='text' name="end_date" class="datetimepickergb  form-control" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" >criar evento</button>
                </div>
            </form>
          </div>
        </div>
      </div>
      <script>
          moment.updateLocale('en', {
            months : [
                "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
            ],
            weekdays : [
                "Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sabado"
            ],
            weekdaysShort : ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
            weekdaysMin : ["D", "S", "T", "Q", "Q", "S", "S"]
        });
        $(function () {
          $('#example1').datetimepicker({
            format:"D-M-Y HH:mm"
          });
        });
        $(function () {
          $('#example2').datetimepicker({
            format:"D-M-Y HH:mm"
  
          });
        });       

        $(function () {
          $('.datetimepickergb ').datetimepicker({
            format:"D-M-Y HH:mm"
          });
        });
      </script>
      
      <style type="text/css">
      @media only screen and (max-width: 530px) {
        .fc-day-grid-container{
        all:unset;
        /* height: 340px;  */
        }
        .fc-widget-content{
          /* all:unset;  */
          height:66px; 
          
        }
        .fc-agendaWeek-view{
          all:unset;
        }
        .fc-view-container{
          all:unset;
        }
        .fc-agendaDay-view{
          all:unset;
        }
      } 

     
      </style>
@stop