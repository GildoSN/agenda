@extends('navbar')
<body onchange="funcaowid()">

@section('content')
  <div class="container">
    <div class="row">
      @if (count($errors) > 0 )
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      <div class="text-center">
          <h1>Meus Contatos</h1>
      </div>
    </div>
    <div class="row">
      @if($contacts->count() > 9)
        <div class="col-md-3 col-sm-12 pull-right">
          <form action="/search" method="GET">
            <label for="search">Procure um contato:</label>
            <div class="input-group">
              <input type="search" placeholder="nome do contato:" name="search" class="form-control">
              <span class="input-group-btn">
                  <button type="submit" class="btn btn-primary" >
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                  </button>
              </span>
            </div>
          </form>
        </div>
      @endif

      <a style="margin-top:2%;" href="{{ route('contacts.excel') }}" class="btn btn-success ">Exportar em Excel</a>

      <button style="margin-top:2%;" class="btn btn-info" onclick="myFunction()">Importar contatos da google</button>
    </div>

    <div class="row">
      <div class="table-responsive">
        <table class="table table-striped " >
          @if($contacts->count() > 0)
            <thead>
              <tr>
                <th class="text-center">Nome</th>
                <th class="text-center">Email</th>
              </tr>
            </thead>
          @endif
          <tbody>
            @forelse ($contacts as $contact)
              <tr class="text-center">
                <td style="vertical-align: middle;">{{$contact->name }}</td>
                <td style="vertical-align: middle;">{{$contact->email }}</td>
                <td>
                  <label style="vertical-align: middle;">
                    <a style="text-decoration: none;" href="/contacts/{{ $contact->id }}/edit">
                      <button class="btn btn-warning" type="button">
                        editar
                      </button>
                    </a>
                  </label>

                  <label style="vertical-align: middle;">
                    <form action="/block/{{ $contact->id }}" method="POST">
                      @method('PUT')
                      @csrf
                      <button name="blockUserId" value="{{ $contact->id }}" type="btn-submit" class="btn"
                        onclick='return confirm("Tem certeza que deseja bloquear esse contato? ")' >
                        bloquear
                      </button>
                    </form>
                  </label>

                  <label disabled style="vertical-align: middle;">
                      {!!Form::open(['action' => ['ContactController@destroy', $contact->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                        {{Form::hidden('_method', 'DELETE')}}
                        {{  Form::submit('Deletar', ['class' => 'btn btn-danger','onclick'=>"return confirm('Tem certeza que deseja deletar esse contato?')"])}}
                      {!!  Form::close()!!}
                  </label>

                  @if($contact->favorite==1)
                    <form action="/contacts/favorite/{{ $contact->id }}" style="margin-right:8%;" class="pull-right" method="GET">
                      <label class="container2">
                        <input style="display:none" onchange="this.form.submit()" type="checkbox" name="{{ $contact->name }}" value="{{$contact->id}}">
                        <span checked type="checkbox" value="aoaoao" id="{{ $contact->id }}" class="star glyphicon glyphicon-star"></span>
                      </label>
                    </form>
                  @endif

                  @if($contact->favorite==0)
                        <form action="/contacts/favorite/{{ $contact->id }}" style="margin-right:8%;" class="pull-right" method="GET">
                          <label class="container2">
                            <input style="display:none" onchange="this.form.submit()" type="checkbox" name="{{ $contact->name }}" value="{{$contact->id}}">
                            <span type="checkbox" value="aoaoao" id="{{ $contact->id }}" class="star glyphicon glyphicon-star-empty"></span>
                          </label>
                        </form>
                    </td>
                  @endif
                </td>
              </tr>
            @empty
                <p>nenhum contato registrado :)</p>
            @endforelse
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="container-fluid" id="linha">
          <span class="pull-left" id="pagi">{{ $contacts->links() }}</span>

          <a class="col-md-6" id="cria_contact" class=" nav-link" href="contacts/create">
            <button class="btn btn-primary" id="cria_contato_up_row" type="button">criar novo contato</button>
          </a>
      </div>
    </div>
    <div class="row">
      <a id="cria_contact" class=" nav-link" href="contacts/create">
        <button class="btn btn-primary" id="cria_contato_down_row" type="button">criar novo contato</button>
      </a>
      <a class="nav-link" style=" color: white; border" href="contacts/block">
        <button  style="background-color:grey;" id="block_contact" class="btn pull-right" type="button">contatos bloqueados</button>
      </a>
    </div>
  </div>
</body>
  <script src="/js/contacts.js"></script>
  <link rel="stylesheet" type="text/css"  href="{!!  asset('/css/contacts.css') !!}" type="text/css">
@stop
