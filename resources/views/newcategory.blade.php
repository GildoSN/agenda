@extends('navbar')
@section('content')

    <div class="container">

        <h1 style="text-align: center">Criação de categoria</h1>

        {!! Form::open(['route' => 'categories.store']) !!}
        
            <div class="form-row" >

                <div class="col-md-12 text-center button-margin">
                    <div class="col-md-6">
                        <label for="category"><h2>Nova categoria</h2></label>
                        <input type="text" name="category" class="form-control" id="category" placeholder="categoria" required>
                        <button class="btn btn-primary" type="submit" style="margin-top:5%;">Criar categoria</button>
                    </div>
        {!!Form::close()!!}
                    <div class="col-md-6">
                        <label for="category"><h2>Suas Categorias</h2></label>

                        <table class="table table-striped" >
                            <tbody>
                                @forelse($category as $id=>$categorie)
                                        <tr class="text-center">
                                            <td style="vertical-align: middle;">{{$categorie }}</td>
                                            <td>
                                                <button type="button" class="edit_button btn btn-warning" 
                                                data-category_id="{{ $id }}" data-mycategory="{{ $categorie }}" 
                                                data-toggle="modal" data-target=".edit">
                                                    editar
                                                </button>

                                                {!!Form::open(['action' => ['CategoryController@destroy', $id],
                                                'method' => 'POST', 'class' => 'pull-right'])!!}

                                                {{Form::hidden('_method', 'DELETE')}}
                                                {{Form::submit('Deletar', ['class' => 'btn btn-danger'])}}
                                                {!!Form::close()!!}
                                            </td>
                                        </tr>
                                        @empty
                                        <p>você não tem categorias</p>

                                                <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> <p> crie uma ao lado :)</p>
                                @endforelse
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>

            <div class="edit modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h3 class="modal-title">Edite sua Categoria abaixo</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <form action="{{ route('categories.update','test' )}}" method="POST">
                            {{ method_field('patch')}}
                            {{ csrf_field() }}
                            <div class="modal-body">
                                <input type="text" id="categoryy" name="category" placeholder="categoria" class="categoria form-control" 
                                required="required"/>
                                <input type="hidden" id="cat_id" name="category_id"  class=" form-control"/>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Sair</button>
                                <button type="submit" class="btn btn-primary" >Salvar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
<script type="text/javascript" src="/js/newcategory.js"></script>
                
<style>
@media only screen and (min-width: 284px) and (max-width:348px) {
    .edit_button{
        margin-left: 35%;
    }
}
</style>
@stop