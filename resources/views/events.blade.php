@extends('navbar')
@section('content')

<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>

    <script src="https://cdn.jsdelivr.net/momentjs/2.14.1/moment.min.js"></script>
     
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
     
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
</head>

<div class="table-responsive">
  <table class="table table-striped " >
    @if($events->count() > 0)
      <thead>
        <tr>
          <th class="text-center">Evento</th>
          <th class="text-center">Inicio</th>
          <th class="text-center">Fim</th>
        </tr>
      </thead>
    
    @endif
    <tbody>
      <p hidden>{{ $i = 0 }}</p>
      @forelse ($events as $event)
      <tr class="text-center">
        <td style="vertical-align: middle;">{{$event->title }}</td>
        <td style="vertical-align: middle;">{{ $event_inicio[$i] }}</td>
        <td style="vertical-align: middle;">{{ $event_fim[$i] }}</td>
        <td>

            <button type="button" class="btn btn-warning" 
            data-event_id="{{ $event->id }}" 
            data-event_title="{{ $event->title }}" 
            data-start_date="{{ $event_inicio[$i] }}" 
            data-end_date="{{ $event_fim[$i] }}"
            data-toggle="modal" data-target=".edit">
                editar
            </button>
            {!!Form::open(['action' => ['EventController@destroy', $event->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
              {{Form::hidden('_method', 'DELETE')}}
              {{Form::submit('Deletar', ['class' => 'btn btn-danger','onclick'=>"return confirm('Tem certeza que deseja deletar esse evento?')"])}}
            {!!Form::close()!!}
          </td>
      </tr>
      <p hidden>{{ $i = $i+1 }}</p>
      

      @empty
      <div class="container">
          <h1>nenhum evento registrado :)</h1>
      </div>
      @endforelse
    </tbody>
  </table>
</div>


<div class="edit modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title">Edite sua Categoria abaixo</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('dashboard.update','test' )}}" method="POST">
            {{ method_field('patch')}}
            {{ csrf_field() }}
      

            <div class="modal-body">
                <label for="categoryy">Evento</label>
                <input type="text" id="categoryy" name="event_title_edit" placeholder="titulo do evento" class="evento form-control" required="required"/>
               
                <div class='col-sm-6'>
                    <div class="form-group">
                        <label for="start_date">Inicio</label>
                        <div class='input-group date' id='example1' >
                            <input type='text' id="start_date" name="start_date" class="datetimepickergb form-control" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                        <input type="hidden" id="cat_id" name="event_id"  class=" form-control"/>

                <div class="form-group">
                    <label for="">Fim</label>
                    <div class='input-group date' id='example2'>
                        <input type='text' id="end_date" name="end_date" class="datetimepickergb form-control" />

                          
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary" >Salvar</button>
            </div>
        </form>
      </div>
    </div>
  </div>

<script type="text/javascript" src="/js/events.js"></script>

@stop

