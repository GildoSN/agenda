@extends('navbar')
@section('content')
<div class="container">

    <div class="row">

      {{-- <div class="container col-lg-4">
        <p>telefones</p>
          <ul class="list-group">
            @forelse ($phones as $phone)

              <li class="list-group-item">{{ $phone->phone}} {{ $phone->contact_id  }} </li>

              @empty
                  <p>nenhum contato registrado :)</p>
            @endforelse

          </ul>
        </div> --}}
      <div class="table-responsive">
          @if (count($errors) > 0 )
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif
      @if($blockedContacts->count() > 0)
        <table class="table table-striped " >
          <thead>
              <tr>
              <th class="text-center">Nome</th>
              <th class="text-center">Email</th>
              <th></th>
              <th></th>
              </tr>
          </thead>
          <tbody>
            @foreach ($blockedContacts as $contact)
              <tr>
                <td class="text-center" style="vertical-align: middle;">{{$contact->name }}</td>
                <td class="text-center" style="vertical-align: middle;">{{$contact->email }}</td>
                <td>
                  <div class="row">
                    <div class="col-md-2">
                      <a style="text-decoration: none;" href="/contacts/{{ $contact->id }}/edit">
                        <button class="btn btn-warning" type="button">
                          edit
                        </button>
                      </a>
                    </div>
                    <div class="col-md-2">
                      <form action="/unblock/{{ $contact->id }}" method="POST">
                        @method('PUT')
                        @csrf
                        <button  name="unblockUserId" value="{{ $contact->id }}"  type="btn-submit" class="btn btn-default">
                          unblock
                        </button>
                      </form>
                    </div>
                  </div>
                </td>
                <td>
                  {!!Form::open(['action' => ['ContactController@destroy', $contact->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                    {{Form::hidden('_method', 'DELETE')}}
                    {{Form::submit('Delete', ['class' => 'btn btn-danger' ,'onclick'=>"return confirm('Tem certeza que deseja deletar esse contato?')"])}}
                  {!!Form::close()!!}
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      @else
        <p>nenhum contato bloqueado :)</p>
      @endif
      </div>
    </div>


@stop