@extends('navbar')
@section('content')
    <div class="container">

        <div class="row">
            @if (count($errors) > 0 )
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="text-center">
            <h1>Excel Contacts</h1>
        </div>

        <div class="row">
            <div class="col-sm-12">
                 @if($contacts->count() > 9)
                    <div class="col-lg-3  pull-right">
                        <form action="/excel/search" method="GET">
                        <p>Procure um contato:</p>
                        <div class="input-group">
                            <input type="search" placeholder="nome do contato:" name="search" class="form-control">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary" >
                                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                                </button>
                            </span>
                        </div>
                        </form>
                    </div>
                @endif

                <button type="button" style="margin-top:3%;" class="btn btn-info" onclick="myFunction()">Importar contatos da google</button>
                {!! Form::open(['route' => 'export_excel.excel', 'id'=>'myForm']) !!}
                {{ csrf_field() }}
            </div>
        </div>
        <div class="row">
            <div class="table-responsive">
                <table class="table table-striped " >
                    @if($contacts->count() > 0)
                        <thead>
                            <tr>
                                <th class="text-center">Nome</th>
                                <th class="text-center">Email</th>
                                <th style="text-align:center;"><input type="checkbox" style="vertical-align: middle; " onclick="toggle_inputs(this)" class="check" id="checkAll"></th>
                            </tr>
                        </thead>
                    @endif
                    <tbody>
                        @forelse ($contacts as $contact)
                            <tr class="text-center">

                                <input name="contactid[]"  disabled class="contact_id" id="contact-id" value="{{$contact->id}}" type="hidden">
                                <td style="vertical-align: middle;"><input id="inp" type="text" class="name" name="name[]" disabled  value="{{$contact->name }}"></td>
                                <td style="vertical-align: middle;"><input id="inp" class="email" type="text" name="email[]" disabled  value="{{$contact->email }}"></td>
                                <td><input type="checkbox" id="checkb" name="contact[]" onchange="enable(this)"  class="check"></td>

                            </tr>
                        @empty
                            <p>nenhum contato registrado :)</p>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <span id="pagi" class="line">{{ $contacts->links() }}</span>

            <a href="{{ route('export_excel.excel') }}">
                <button type="submit" class="line btn btn-success pull-right">Exportar</button>
            </a>
    </div>
    {!! Form::close() !!}
    <script src="/js/contactexcel.js"></script>

    <style>

        .line{
            display: inline-block;

        }
        .pagination{
            margin:0;
        }

        input[type=text] {
            border: none;
            background-color: transparent;
        }
    </style>

@stop