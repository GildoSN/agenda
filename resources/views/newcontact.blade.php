@extends('navbar')
@section('content')
    <script>
        $(document).ready(function(){
             var i=1;
             $('#add').click(function(){
                i++;
                $('#dynamic_field').append('<tr id="row'+i+'"><td><input type="text" pattern=".{15,19}" style="margin-bottom: 3px;" class="phone form-control" id="phone" name="phone[]" placeholder="Telefone" class="form-control" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
                var maskBehavior = function (val) {
                    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-000000' : '(00)(00) 0000-00009';
                    },
                    options = {onKeyPress: function(val, e, field, options) {
                            field.mask(maskBehavior.apply({}, arguments), options);
                        }
                };
                $('.phone').mask(maskBehavior, options);
             });
             $(document).on('click', '.btn_remove', function(){
                  var button_id = $(this).attr("id");
                  $('#row'+button_id+'').remove();
             });
        });
    </script>
    <script type="text/javascript" src="/js/address.js"></script>
</head>
<body>
    <div class="container teste1">
        <h1>Criação de contato</h1>
        {!! Form::open(['route' => 'contacts.store', 'id'=>'myForm']) !!}
        {{ csrf_field() }}
        @if (count($errors) > 0 )
            <div class="alert alert-danger">

                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-4">
                <label class="gildo" for="name">Nome</label>
                <div class="input">
                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="nome do contato" required="required">
                </div><!-- /input -->
            </div><!-- /.col-lg-6 -->

            <div class="col-lg-4">
                <label class="gildo">Telefone: </label>
                <div class="form-group">
                    <div class="table-responsive">
                        <table id="dynamic_field" style="width: 100%; ">
                            @if(old('phone'))
                                @foreach(old('phone') as $phone)
                                    <tr>
                                        <td>
                                            <input type="text" style="margin-bottom: 3px;" value="{{ $phone }}"  pattern=".{15,19}" id="phone" name="phone[]" placeholder="Telefone" class="phone form-control" required="required"/>
                                        </td>
                                        <td>
                                            <button type="button" name="add" id="add" class="btn btn-info">+</button>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <td>
                                    <input type="text" style="margin-bottom: 3px;" pattern=".{15,19}" id="phone" name="phone[]" placeholder="Telefone" class="phone form-control" required="required"/>
                                </td>
                                <td>
                                    <button type="button" name="add" id="add" class="btn btn-info">+</button>
                                </td>
                            @endif
                        </table>
                    </div>
                </div>
            </div><!-- /.col-lg-6 -->

            <div id="refresh" class="col-lg-4">

                <label class="gildo" for="categoria">Categoria</label>
                {{-- {!! Form::Label('category','categoria:') !!} --}}
                <div class="btn-group-lg" >
                    {!! Form::select('category[]',
                        $category

                    , null, ['class' =>'categorias form-control','id'=>'multiple-checkboxes','multiple'=>'multiple']) !!}

                    <span class="btn-group" >
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModalCenter">
                            Nova categoria
                        </button>
                    </span>

                </div><!-- /input-group -->

            </div><!-- /.col-lg-6 -->
        </div>

        <div class="row">
            <div class="col-lg-4">
                <label class="gildo" for="cpf">CPF</label>
                <div class="input">
                    <input type="text" class="cpf form-control" id="cpf" name="cpf"  value="{{ old('cpf') }}" placeholder="CPF">
                </div><!-- /input -->
            </div><!-- /.col-lg-6 -->

            <div class="col-lg-4">
                <label class="gildo" for="rg">RG</label>
                <div class="input">
                    <input type="text" class="rg form-control" id="rg" name="rg"  value="{{ old('rg') }}" placeholder="RG">
                </div><!-- /input -->
            </div><!-- /.col-lg-6 -->

            <div class="col-lg-4">
                <label class="gildo" for="email">Email</label>
                <input name="email" type="email" class="form-control" id="email" value="{{ old('email') }}" placeholder="Endereço de email">
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <H2 id="endere">Endereço</H2>
                <div class="col-md-12 text-center button-margin">

                <button style="margin-bottom:20px;" type="button" onclick="clone()" class="btn btn-info">
                    Adicionar Endereços
                </button>
                </div>
                <div id="address">
                    <div class="address_container col-md-12" >
                        <div class="row">
                            <div class="col-xs-6 col-md-4">
                                <label>Cep:
                                <input name="addresses[0][cep]" pattern=".{9,9}" value="{{ old('addresses.0.cep') }}" required="required" id="cep" class="cep form-control"  type="text" value="" size="100" maxlength="9" onblur="pesquisacep(this);" placeholder="CEP" /></label>
                            </div>

                            <div class="col-xs-6 col-md-4">
                                <label>Rua:
                                <input name="addresses[0][logradouro]"  value="{{ old('addresses.0.logradouro') }}" required="required" class="form-control" id="logradouro"  type="text" size="60" placeholder="Rua"/></label>
                            </div>
                            <div class="col-xs-6 col-md-4">
                                <label>Complemento:
                                <input type="text" class="form-control" name="addresses[0][complemento]" id="complemento"  value="{{ old('addresses.0.complemento') }}" required="required" placeholder="complemento" size="60">
                                </label>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-md-4">
                                <label>Bairro:
                                <input name="addresses[0][bairro]"  value="{{ old('addresses.0.bairro') }}" id="bairro" required="required" class="form-control"  type="text" size="100" placeholder="Bairro"/>
                            </div>

                            <div class="col-xs-6 col-md-4">
                                <label>Cidade:
                                <input name="addresses[0][localidade]"  value="{{ old('addresses.0.localidade') }}" id="localidade" required="required" class="form-control"  type="text" size="40" placeholder="Cidade"/></label>
                            </div>
                            <div class="col-xs-6 col-md-4">
                                <label>Estado:
                                <input name="addresses[0][uf]"  value="{{ old('addresses.0.uf') }}" id="uf" required="required" class="uf form-control"  type="text" size="2" placeholder="UF"/></label>
                                <label>IBGE:
                                <input name="addresses[0][ibge]"  value="{{ old('addresses.0.ibge') }}" id="ibge" class="form-control"  type="text" size="8" placeholder="IBGE"/></label>
                                <label>Unidade:
                                <input type="text" class="form-control" name="addresses[0][unidade]" value="{{ old('addresses.0.unidade') }}" id="unidade" size="8" placeholder="unidade"></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="col-md-12 text-center button-margin">
                <button id="myForm" class="btn btn-primary" type="submit">Criar contato</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="exampleModalCenterTitle">Adicione uma nova categoria</h2>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="cria_categoria">
                            {{ csrf_field() }}
                            <div class="form-row" >
                                <div class="form-group" style="margin-bottom: -15px;">
                                    <label for="category">
                                        Nova categoria
                                    </label>
                                    <input type="text" name="category" class="form-control" id="category" placeholder="categoria">
                                </div>
                            </div>
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Sair</button>
                            <button id="cria-categoria" class=" btn btn-primary" type="button">
                                Criar categoria
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
<script type="text/javascript" src="/js/newcontact.js"></script>
@stop