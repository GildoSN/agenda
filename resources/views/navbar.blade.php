<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        {{-- <link rel="shortcut icon" href="{{ asset('https://img.icons8.com/cotton/64/000000/calendar.png') }}"> --}}
        <title>Agenda</title>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <style>
            .gildo{
                margin-top:15px;
            }
            p{
                margin-top:5px;
            }

            #logout_button , #perfil_button {
            display:inline-block;
            /**other codes**/
            }
            .navbar{
                border-radius: 0;
            }

            .container{
                background-color: rgba(0,0,0,0.1);
                padding:20px;
                border:2px solid;
                border-color:rgba(0,0,0,0.25 );
            }

            .button-margin{
                margin-top:20px;
            }

            .center-block {
                display: block;
                margin-left: auto;
                margin-right: auto;
            }
            #endere{
                text-align: center;
            }

            form{
                margin:0;
            }

        </style>
    </head>
    <body>
        <nav class="navbar navbar-inverse" style="margin-bottom:0px">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/dashboard">Eventos</a>
                </div>

                <div class="collapse navbar-collapse"  id="bs-example-navbar-collapse-1">

                    <ul class="nav navbar-nav" >
                    <li><a href="/contacts">Contatos</a></li>
                    <li><a href="/categories">Categoria</a></li>
                    <li><a href="/suporte">Suporte</a></li>
                    </ul>

                    {{-- <form method="POST" class="pull-right" action='/logout'>
                        @csrf --}}

                    <a href="/logout"> <button style="margin-top:0.6%; margin-left:0.5%; " id="logout_button" class="pull-right btn btn-sm btn-danger"
                        style="vertical-align: middle;"  type="button" >
                            Sair
                        </button></a>
                    {{-- </form> --}}

                    <button style="vertical-align: middle; margin-top:0.6%;" id="perfil_button"    type="button" class="pull-right btn btn-sm btn-info" data-toggle="modal" data-target="#exampleModal">
                            Perfil
                        </button>
                    <h4 class="pull-right" style="vertical-align: middle; margin-right:10px; margin-top:1%; color:white">
                        Logado como( {{ Auth::user()->user_name }} )
                    </h4>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <div class="pull-right col-md-2">
            @include('flash::message')
        </div>
        @yield('content')
    </body>
</html>


<script>
    function copia_api_token() {
        /* Get the text field */
        console.log("foi");
        var copyText = document.getElementById("api_token");

        /* Select the text field */
        copyText.select();
        /* Copy the text inside the text field */
        document.execCommand("copy");

        /* Alert the copied text */
        alert("Texto copiado: " + copyText.value);
        }
</script>
<!-- Modal -->
<div class="modal fade"  id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:86%; margin-left:14%;">
            <div class="modal-header">
            <h2 class="modal-title" id="exampleModalLabel">Perfil</h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body" >
            <form action="{{ route('profile.update','test' )}}" method="POST">
            {{ method_field('patch')}}
            {{ csrf_field() }}
            <p>Nome:
                <div class="input-group">
                    <input id="name" name="user_name" type="text" class="form-control" disabled value="{{ auth()->user()->user_name }}">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-warning" onclick="edit_name()">editar</button>
                    </span>
                </div><!-- /input-group -->
            </p>
            <p>Email:
                <div class="input-group">
                    <input id="userEmail" type="email" name="email" class="form-control" disabled value="{{ auth()->user()->email }}">

                    <span class="input-group-btn">
                    <button type="button" onclick="edit_email()" class="btn btn-warning">editar</button>

                    </span>
                </div><!-- /input-group -->
            </p>
            <input type="text" hidden name="id" value="{{ auth()->user()->id }}">
            <p>
                <button class="btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    API-Key
                </button>
            </p>

            <div class="collapse" id="collapseExample">

                <input  id="api_token" class="form-control" type="text" value="{{ auth()->user()->api_token }}">

                <button type="button" onclick="copia_api_token()" class="btn btn-info">
                    <span class="glyphicon glyphicon-copy" aria-hidden="true">copiar</span>
                </button>
            </div>

            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Sair</button>

                <button type="submit" class="btn btn-primary">Salvar</button>
            </form>
            </div>
        </div>
    </div>
    <script>
        $('div.alert').not('.alert-important').delay(2000).fadeOut(450);
    </script>
</div>

<script type="text/javascript" src="/js/navbar.js"></script>



