@extends('navbar')
@section('content')
<body>
    {!! Form::open(['route' => 'suporte.store', 'id'=>'myForm']) !!}
    {{ csrf_field() }}
    @if (count($errors) > 0 )
        <div class="alert alert-danger">

            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>    
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container">
        <h1>Suporte</h1>
        <div class="form-group">
            <label for="exampleFormControlInput1">Email</label>
            <input type="email" class="form-control" name="email" id="email" placeholder="name@example.com">
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">Assunto</label>
            <input type="text" class="form-control" id="assunto" name="assunto" placeholder="Assunto">
        </div>
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Mensagem</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" name="texto" placeholder="Mensagem que deseja enviar" rows="3"></textarea>
        </div>
        <div style="text-align:center">
            <button class="btn btn-primary" type="submit">enviar</button>
        </div>
    </div>


    {!! Form::close() !!}

</body>
@stop