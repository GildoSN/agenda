<?php

Route::resource('profile', 'UserController')->middleware('auth');
Route::post('register/user', 'UserController@store');
Route::get('register', 'UserController@register');
// Route::get('/profile','UserController@index')->middleware('auth');

Route::get('/', 'AuthenticationController@login')->name('loginPage');
Route::post('login', 'AuthenticationController@authenticate');
Route::get('/logout', 'AuthenticationController@logout');

Route::resource('contacts', 'ContactController')->middleware('auth');
Route::get('/excel', 'ContactController@excelIndex')->middleware('auth')->name('contacts.excel');
Route::get('/contacts/favorite/{id}', 'ContactController@favorite')->name('contacts.favorite');
Route::get('/search', 'ContactController@search')->middleware('auth');
Route::put('block/{id}', 'ContactController@block')->name('block');
Route::put('unblock/{id}', 'ContactController@unblock')->name('unblock');

Route::post('/verificanew', 'VerifyEmailController@sendNewVerifyEmail');
Route::get('login/token/{token}', 'VerifyEmailController@verifyEmail');
Route::get('/verify/email', 'VerifyEmailController@index');

Route::resource('suporte', 'SuporteController')->middleware('auth');

Route::get('/excel/search', 'ExportExcelController@search')->middleware('auth');
Route::post('/export_excel/excel', 'ExportExcelController@excel')->name('export_excel.excel');
Route::get('/export_excel', 'ExportExcelController@index')->middleware('auth');

Route::resource('categories', 'CategoryController')->middleware('auth');

Route::get('dashboard', 'EventController@index')->middleware('auth');
Route::post('dashboard.store', 'EventController@store')->middleware('auth')->name("dashboard.store");
Route::get('/dashboard/events', 'EventController@eventsView')->middleware('auth')->name("dashboard.events");
Route::patch('/dashboard/events/{id}', 'EventController@update')->middleware('auth')->name("dashboard.update");
Route::DELETE('/dashboard/events/{id}', 'EventController@destroy')->middleware('auth')->name("dashboard.destroy");

Route::DELETE('/phone/delete/{id}', 'PhoneController@destroy')->middleware('auth')->name("phone.destroy");

Route::get('contact/import/google', ['as' => 'google.import', 'uses' => 'GoogleContactController@importGoogleContact'])
->middleware('auth');

Route::resource('addGoogleContacts', 'GoogleContactController')->middleware('auth');
