<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('welcome', 'AuthController@welcome');

Route::resource('contacts', 'ApiContactController')->middleware('auth:api');

Route::delete('contact/{contact}', 'ApiContactController@destroy')->middleware('auth:api');

Route::put('contact/edit/{contact}', 'ApiContactController@update')->middleware('auth:api');

Route::post('contacts/create', 'ApiContactController@store')->middleware('auth:api');

// Route::put('contact', 'ApiContactController@store');


// Route::resource('contactsaddress', 'AuthController');

// route::get('block','ContactController@block')->name('block');
// route::get('unblock','ContactController@unblock')->name('unblock');


// Route::resource('categories', 'CategoryController');



// Route::get('register', 'AuthController@register');

// Route::post('register/user', 'AuthController@store'); 




// Route::get('/', 'LoginController@login')->name('loginPage');

// Route::post('login', 'LoginController@authenticate')->middleware('auth:api');

// Route::post('logout', 'LoginController@logout');




// Route::get('dashboard', 'AuthController@dashboard');


