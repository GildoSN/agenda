<?php

namespace App\Rules;

use App\Models\Contact;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Validation\Rule;

class ValidContactEmailUpdate implements Rule
{
    // public $id;
    /**
     * Create a new rule instance. 
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(!is_null($value)){
            $contact = Contact::find(request('contact'));
            // dd($contact->email);
            $copia_email= Contact::where([
                ['email',$value],
                ['users_id',Auth::user()->id],
            ])->get();
            $contagem = count($copia_email);
            foreach($copia_email as $key=>$value){ 
                if($value->id==$contact->id){
                    $contagem = $contagem - 1;
                }
            }
            // dd($contagem);

            if($contagem == 0){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Voce ja tem um contato com esse email.';
    }
}
