<?php

namespace App\Rules;

use App\Models\Phone;
use App\Models\Contact;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Validation\Rule;

class ValidContactPhone implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $contact = Contact::find(request('contact'));
        // dd(request('phone'));
        $id_user = Auth::user()->id;
        // dd($id_user);
        foreach(request('phone') as $phone)
        {
            // dd($phone);
            $user_phones = Phone::where([
                ['phone','=',$phone],
                ['user_id','=',$id_user],
            ])->get();
        }
        // dd($user_phones);
        $contagem = count($user_phones);

        // dd($user_phones);
        if(!is_null($contact)){
            foreach($user_phones as $key=>$value){
                if($value->contact_id==$contact->id){
                    $contagem = $contagem - 1;
                }
            }
            if($contagem == 0){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            if($contagem == 0){
                return true;
            }
            else{
                return false;
            }
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Você ja tem um contato com esse telefone.';
    }
}
