<?php

namespace App\Rules;

use App\Models\Contact;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Validation\Rule;

class ValidContactCPFUpdate implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // dd(request('contact'));
        try{
            $contact = Contact::find(request('contact'));
            // dd($contact);
            // dd($value);
            // dd(Auth::user()->id);
            // dd($this->x_api_key);
            if(is_null($value)){
                return true;
            }
            $copia_cpf= Contact::where([
                ['cpf',$value],
                ['users_id',Auth::user()->id],
            ])->get();

            $contagem = count($copia_cpf);
            foreach($copia_cpf as $key=>$value){
                if($value->id==$contact->id){
                    $contagem = $contagem - 1;
                }
            }
            if($contagem == 0){
                return true;
            }
            else{
                return false;
            }
        } catch(Exception $e) {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'você ja tem um contato com esse cpf.';
    }
}
