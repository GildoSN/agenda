<?php

namespace App\Rules;

use App\Models\Phone;
use App\Models\Contact;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Validation\Rule;

class ValidNewPhone implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $contact = Contact::find(request('contact'));
        // dd(request());
        $id_user = Auth::user()->id;
        if(!is_null(request('newphone'))){
            foreach(request('newphone') as $key=>$newphone){
                $user_newphones = Phone::where([
                    ['phone','=',$newphone],
                    ['user_id','=',$id_user],
                ])->get();
                
            }
        }
        if($user_newphones->isEmpty()){
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Você ja tem um contato com esse telefone.';
    }
}
