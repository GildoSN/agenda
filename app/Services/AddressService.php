<?php

namespace App\Services;

use App\Models\Address;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repositories\AddressRepository;
use App\Services\Responses\ServiceResponse;
use App\Services\Contracts\AddressServiceInterface;

class AddressService implements AddressServiceInterface
{
    /**
     * @var AddressRepository
     */
    private $addressRepository;

    /**
     * @param AddressRepository $addressRepository
     */
    public function __construct(AddressRepository $addressRepository)
    {
        $this->addressRepository = $addressRepository;
    }

    /**
     * Returns contact by id
     * @param array $params
     *
     * @return ServiceResponse
     */
    public function create(array $params): ServiceResponse
    {
        DB::beginTransaction();
        try {
            $address = $this->addressRepository->create($params);
        } catch (Exception $e) {
            DB::rollback();
            return new ServiceResponse(true, 'Erro ao criar endereço', null);
        }
        DB::commit();
        return new ServiceResponse(true, 'usuarios obtidos com sucesso', $address);
    }

    /**
     * Returns contact by id
     * @param int $contactId
     * @param int $addressId
     *
     * @return ServiceResponse
     */
    public function find(int $contactId, int $addressId): ServiceResponse
    {
        try {
            $address = $this->addressRepository
            ->findWhere(['contact_id' => $contactId, 'id' => $addressId])
            ->first();
            if (!isset($address)) {
                return new ServiceResponse(false, 'Endereço inexistente', null);
            }
        } catch (Exception $e) {
            return new ServiceResponse(false, 'Erro ao procurar endereço', null);
        }
        return new ServiceResponse(true, 'usuarios obtidos com sucesso', $address);
    }
}
