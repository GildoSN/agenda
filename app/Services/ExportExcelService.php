<?php

namespace App\Services;

use Excel;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Repositories\LoginTokenRepository;
use App\Services\Responses\ServiceResponse;
use App\Services\Contracts\LoginTokenServiceInterface;

class ExportExcelService
{
    /**
     * Returns contact by id
     * @param int $userId
     * @param string $loginToken
     *
     * @return ServiceResponse
     */
    public function contactsToExcel($contacts): ServiceResponse
    {
        try {
            $contactArray = [];
            $contactArray[] = array('id', 'Contact Name', 'Cpf', 'Rg', 'Email', 'Dado', 'Valor');
            foreach ($contacts as $contact) {
                foreach ($contact->addresses as $address) {
                    $contactArray[] = array(
                        'id'           => $contact->id,
                        'Contact Name' => $contact->name,
                        'Cpf'          => $contact->cpf,
                        'RG'           => $contact->rg,
                        'Email'        => $contact->email,
                        'dado'         => 'endereço',
                        'valor'        => $address->logradouro . ', ' . $address->bairro . ', ' . $address->complemento,
                    );
                }
                $categorias = join(', ', $contact->categories->pluck('category')->toarray());
                $contactArray[] = array(
                    'id'           => $contact->id,
                    'Contact Name' => $contact->name,
                    'Cpf'          => $contact->cpf,
                    'RG'           => $contact->rg,
                    'Email'        => $contact->email,
                    'dado'         => 'categorias',
                    'valor'        => $categorias,
                );
                foreach ($contact->phones as $phone) {
                    $contactArray[] = array(
                        'id'           => $contact->id,
                        'Contact Name' => $contact->name,
                        'Cpf'          => $contact->cpf,
                        'RG'           => $contact->rg,
                        'Email'        => $contact->email,
                        'dado'         => 'telefone',
                        'valor'        => $phone->phone,

                    );
                }
            }
            $excelObject = Excel::create('Contatos Em Excel', function ($excel) use ($contactArray) {
                $excel->setTitle('Contatos Em Excel');
                $excel->sheet('Contatos Em Excel', function ($sheet) use ($contactArray) {
                    $sheet->fromArray($contactArray, null, 'A1', false, false);
                });
            });
        } catch (Exception $e) {
            return new ServiceResponse(true, 'Erro ao converter contatos para array excel', null);
        }
        return new ServiceResponse(true, 'Contatos exportados com sucesso', $excelObject);
    }
}
