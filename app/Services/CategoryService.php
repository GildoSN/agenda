<?php

namespace App\Services;

use Exception;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use App\Repositories\ContactRepository;
use Illuminate\Support\ServiceProvider;
use App\Repositories\CategoryRepository;
use App\Services\Responses\ServiceResponse;
use App\Services\Contracts\CategoryServiceInterface;

class CategoryService implements CategoryServiceInterface
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Returns all the categories from user id passed
     * @param int $id
     *
     * @return ServiceResponse
     */
    public function all(int $id): ServiceResponse
    {
        try {
            $categories = $this->categoryRepository->findWhere(['user_id' => $id]);
        } catch (Exception $e) {
            return new ServiceResponse(false, 'Erro ao procurar categorias', null);
        }
        return new ServiceResponse(true, 'Categorias obtidas com sucesso', $categories);
    }

    /**
     * Creates a category
     * @param array $params
     *
     * @return ServiceResponse
     */
    public function create(array $params): ServiceResponse
    {
        DB::beginTransaction();
        try {
            $this->categoryRepository->create($params);
        } catch (Exception $e) {
            DB::rollback();
            return new ServiceResponse(false, 'Erro ao criar categoria', null);
        }
        DB::commit();
        return new ServiceResponse(true, 'Categoria criada com sucesso');
    }

    /**
     * Updates a category
     * @param array $inputs
     * @param int $id
     *
     * @return ServiceResponse
     */
    public function update(array $inputs, int $id): ServiceResponse
    {
        DB::beginTransaction();
        try {
            $category = $this->categoryRepository->update($inputs, $id);
        } catch (\Throwable $th) {
            DB::rollback();
            return new ServiceResponse(false, 'Erro ao atualizar categoria!', null);
        }
        DB::commit();
        return new ServiceResponse(true, 'Categoria atualizada com sucesso!', $category);
    }


    /**
     * Deletes category from database
     * @param int $userId
     * @param int $idCategory
     *
     * @return ServiceResponse
     */
    public function delete(int $userId, int $idCategory): ServiceResponse
    {
        DB::beginTransaction();
        try {
            $category = $this->categoryRepository->deleteWhere(['user_id' => $userId,'id' => $idCategory]);
            if ($category == 0) {
                DB::rollback();
                return new ServiceResponse(false, 'Categoria inválida', null);
            }
        } catch (Exception $e) {
            DB::rollback();
            return new ServiceResponse(false, 'Erro ao deletar categoria', null);
        }
        DB::commit();
        return new ServiceResponse(true, 'Categoria removida com sucesso', $category);
    }
}
