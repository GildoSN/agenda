<?php

namespace App\Services;

use Exception;
use App\Models\LoginToken;
use Illuminate\Support\Str;
use App\Events\ConfirmEmailEvent;
use Illuminate\Support\Facades\DB;
use App\Services\Responses\ServiceResponse;
use App\Repositories\UserRepositoryEloquent;

class UserService
{
    /** @var UserRepositoryEloquent */
    protected $userRepositoryEloquent;

    public function __construct(UserRepositoryEloquent $userRepositoryEloquent)
    {
        $this->userRepositoryEloquent = $userRepositoryEloquent;
    }

    /**
     * Returns user by id
     * @param int $userId
     * @param int $iduser
     *
     * @return ServiceResponse
     */
    public function find(int $userId): ServiceResponse
    {
        try {
            $user = $this->userRepositoryEloquent->find($userId)->first();
            if (!isset($user)) {
                return new ServiceResponse(false, 'Usuário inexistente', null);
            }
        } catch (Exception $e) {
            return new ServiceResponse(false, 'Erro ao obter usuário', null);
        }
        return new ServiceResponse(true, 'Usuário obtido com sucesso', $user);
    }

    /**
     * Updates a contact
     * @param int $userId
     * @param int $id
     *
     * @return ServiceResponse
     */
    public function update(array $inputs, int $id): ServiceResponse
    {
        DB::beginTransaction();
        try {
            $response = $this->find($id);
            if (!$response->success) {
                DB::rollback();
                return $response;
            }
            $response = $this->userRepositoryEloquent->update($inputs, $id);
        } catch (\Throwable $th) {
            DB::rollback();
            return new ServiceResponse(false, 'Erro ao atualizar usuário!', null);
        }
        DB::commit();
        return new ServiceResponse(true, 'Usuário atualizado com sucesso!', $response);
    }

    /**
     * Creates a new contact
     * @param array $params
     *
     * @return ServiceResponse
     */
    public function create(array $params): ServiceResponse
    {
        DB::beginTransaction();
        try {
            $params['api_token'] = Str::random(60);
            $user = $this->userRepositoryEloquent->create($params);
            $token = LoginToken::generateFor($user);
            $confirmEmailparams = [
                'email' => $params['email'],
                'url' => url('login/token', $token),
            ];
            event(new ConfirmEmailEvent($confirmEmailparams));
        } catch (Exception $e) {
            DB::rollback();
            return new ServiceResponse(false, 'Erro ao criar usuário!', null);
        }
        DB::commit();
        return new ServiceResponse(true, 'Usuário criado com sucesso!', $user);
    }
}
