<?php

namespace App\Services\Contracts;

use App\Services\Responses\ServiceResponse;

interface PhoneServiceInterface
{
    public function all(int $userId, int $contactId): ServiceResponse;
    public function create(array $params): ServiceResponse;
}
