<?php

namespace App\Services\Contracts;

use App\Services\Responses\ServiceResponse;

interface CategoryServiceInterface
{
    public function all(int $id): ServiceResponse;
    public function create(array $params): ServiceResponse;
    public function update(array $inputs, int $id): ServiceResponse;
    public function delete(int $userId, int $idCategory): ServiceResponse;
}
