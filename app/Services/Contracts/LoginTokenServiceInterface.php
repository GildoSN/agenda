<?php

namespace App\Services\Contracts;

use App\Services\Responses\ServiceResponse;

interface LoginTokenServiceInterface
{
    public function delete(int $userId, string $loginToken): ServiceResponse;
}
