<?php

namespace App\Services\Contracts;

use App\Services\Responses\ServiceResponse;

interface AddressServiceInterface
{
    public function create(array $params): ServiceResponse;
}
