<?php

namespace App\Services\Contracts;

use App\Models\User;
use App\Services\Responses\ServiceResponse;

interface ContactsServiceInterface
{
    public function getContactByStatus(User $user, int $status): ServiceResponse;
    public function searchContactByName(User $user, string $name): ServiceResponse;
    public function find(int $userId, int $idContact): ServiceResponse;
    public function delete(int $userId, int $idContact): ServiceResponse;
    public function favorite(int $userId, int $idContact): ServiceResponse;
    public function blockedUpdate(int $userId, int $idContact, int $status): ServiceResponse;
    public function update(array $inputs, int $id): ServiceResponse;
    public function create(array $params): ServiceResponse;
}
