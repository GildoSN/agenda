<?php

namespace App\Services;

use Exception;
use App\Models\User;
use App\Repositories\PhoneRepository;
use App\Services\Responses\ServiceResponse;
use App\Services\Contracts\PhoneServiceInterface;

class PhoneService implements PhoneServiceInterface
{
    /**
     * @var PhoneRepository
     */
    private $phoneRepository;

    /**
     * @param PhoneRepository $categoryRepository
     */
    public function __construct(PhoneRepository $phoneRepository)
    {
        $this->phoneRepository = $phoneRepository;
    }
    /**
     * Returns all the contacts that are not blocked
     * @param int $userId
     *
     * @return ServiceResponse
     */
    public function all(int $userId, int $contactId): ServiceResponse
    {
        // dd($categories);
        try {
            $phones = $this->phoneRepository->findWhere(['user_id' => $userId, 'contact_id' => $contactId]);
            // dd($phones);
        } catch (Exception $e) {
            dd($e);
            return new ServiceResponse(true, 'ERRO', $phones);
        }
        return new ServiceResponse(true, 'usuarios obtidos com sucesso', $phones);
    }

    /**
     * Returns contact by id
     * @param int $userId
     *
     * @return ServiceResponse
     */
    public function find(int $userId, int $idPhone): ServiceResponse
    {
        try {
            // dd($idPhone);
            $phone = $this->phoneRepository->findWhere(['user_id' => $userId, 'id' => $idPhone])->first();
            // dd($phone);
        } catch (Exception $e) {
            dd($e);
            return new ServiceResponse(true, 'ERRO', $phone);
        }
        return new ServiceResponse(true, 'usuarios obtidos com sucesso', $phone);
    }

    /**
     * Returns contact by id
     * @param array $params
     *
     * @return ServiceResponse
     */
    public function create(array $params): ServiceResponse
    {
        try {
            // dd($params);
            $contact = $this->phoneRepository->create($params);
        } catch (Exception $e) {
            dd($e);
            return new ServiceResponse(true, 'ERRO', $contact);
        }
        return new ServiceResponse(true, 'usuarios obtidos com sucesso', $contact);
    }

    /**
     * Returns contact by id
     * @param int $userId
     *
     * @return ServiceResponse
     */
    public function delete(int $userId, int $idPhone): ServiceResponse
    {
        try {
            // dd($userId);
            $phone = $this->phoneRepository->deleteWhere(['user_id' => $userId,'id' => $idPhone]);
            if ($phone == 0) {
                return new ServiceResponse(false, 'phone inválido', $phone);
            }
            // $contact->delete();
            return new ServiceResponse(true, 'phone removido com sucesso', $phone);
        } catch (Exception $e) {
            dd($e);
            return new ServiceResponse(true, 'ERRO', $phone);
        }
    }
}
