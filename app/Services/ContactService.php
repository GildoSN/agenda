<?php

namespace App\Services;

use Exception;
use App\Models\User;
use App\Services\PhoneService;
use Illuminate\Support\Facades\DB;
use App\Repositories\PhoneRepository;
use App\Repositories\AddressRepository;
use App\Repositories\ContactRepository;
use App\Events\NewContactRegisteredEvent;
use App\Services\Responses\ServiceResponse;
use App\Services\Params\CreatePhoneServiceParams;
use App\Services\Params\CreateAddressServiceParams;
use App\Services\Contracts\ContactsServiceInterface;

class ContactService implements ContactsServiceInterface
{
    /**
     * @var ContactRepository
     */
    private $contactRepository;

    /** @var PhoneService */
    private $phoneService;

    /** @var AddressService */
    private $addressService;

    /**
     * @param ContactRepository $contactRepository
     * @param PhoneService $phoneService
     * @param AddressService    $addressService
     */
    public function __construct(
        contactRepository $contactRepository,
        PhoneService $phoneService,
        AddressService $addressService
    ) {
        $this->contactRepository = $contactRepository;
        $this->phoneService = $phoneService;
        $this->addressService = $addressService;
    }
    /**
     * Returns all the contacts according to status passed
     * @param User $user
     * @param int $status
     * @return ServiceResponse
     */
    public function getContactByStatus(User $user, int $status): ServiceResponse
    {
        try {
            $contacts = $this->contactRepository->getContactByStatus($user, $status);
        } catch (Exception $e) {
            return new ServiceResponse(false, 'Erro ao obter contatos', null);
        }
        return new ServiceResponse(true, 'Contatos obtidos com sucesso', $contacts);
    }

    /**
     * Returns all the contacts filtered by name param
     * @param User $user
     * @param string $name
     * @return ServiceResponse
     */
    public function searchContactByName(User $user, string $name): ServiceResponse
    {
        try {
            $contacts = $this->contactRepository->getContactByName($user, $name);
        } catch (Exception $e) {
            return new ServiceResponse(false, 'Erro ao obter contatos', null);
        }
        return new ServiceResponse(true, 'Contatos obtidos com sucesso', $contacts);
    }

    /**
     * Returns contact by id
     * @param int $userId
     * @param int $idContact
     *
     * @return ServiceResponse
     */
    public function find(int $userId, int $idContact): ServiceResponse
    {
        try {
            $contact = $this->contactRepository
            ->findWhere(['users_id' => $userId, 'id' => $idContact])
            ->first();
            if (!isset($contact)) {
                return new ServiceResponse(false, 'Contato inexistente', null);
            }
        } catch (Exception $e) {
            return new ServiceResponse(false, 'Erro ao obter contato', null);
        }
        return new ServiceResponse(true, 'Contato obtido com sucesso', $contact);
    }

    /**
     * Returns contact by id
     * @param int $userId
     * @param array $idContact
     *
     * @return ServiceResponse
     */
    public function findMultipleContacts(int $userId, array $idContacts): ServiceResponse
    {
        try {
            $contact = $this->contactRepository->findWhereIn('id', $idContacts)->where('users_id', $userId);
            if (!isset($contact)) {
                return new ServiceResponse(false, 'Contato inexistente', null);
            }
        } catch (Exception $e) {
            return new ServiceResponse(false, 'Erro ao obter contato', null);
        }
        return new ServiceResponse(true, 'Contato obtido com sucesso', $contact);
    }

    /**
     * Deletes contact from database
     * @param int $userId
     * @param int $idContact
     *
     * @return ServiceResponse
     */
    public function delete(int $userId, int $idContact): ServiceResponse
    {
        try {
            $contact = $this->contactRepository->deleteWhere(['users_id' => $userId,'id' => $idContact]);
            if ($contact == 0) {
                return new ServiceResponse(false, 'Usuario inválido', null);
            }
        } catch (Exception $e) {
            return new ServiceResponse(false, 'Erro ao deletar contato', null);
        }
        return new ServiceResponse(true, 'usuario removido com sucesso', $contact);
    }

    /**
     * Favorites a contact
     * @param int $userId
     * @param int $idContact
     *
     * @return ServiceResponse
     */
    public function favorite(int $userId, int $idContact): ServiceResponse
    {
        try {
            $response = $this->find($userId, $idContact);
            if (!$response->success) {
                return $response;
            }
            $contact = $response->data;
            $contactFavorite = $contact->favorite;
            $favorite = ['favorite' => $contactFavorite == 0 ? 1 : 0];
            $contact = $this->contactRepository->update($favorite, $idContact);
        } catch (\Throwable $th) {
            return new ServiceResponse(false, 'Erro ao favoritar contato', null);
        }
        return new ServiceResponse(true, 'contato favoritado com sucesso', $contact);
    }

    /**
     * Set contact status according to status passed
     * @param int $userId
     * @param int $idContact
     * @param int $status
     *
     * @return ServiceResponse
     */
    public function blockedUpdate(int $userId, int $idContact, int $status): ServiceResponse
    {
        try {
            $response = $this->find($userId, $idContact);
            if (!$response->success) {
                return $response;
            }
            $contact = $this->contactRepository->update(['status' => $status], $idContact);
        } catch (\Throwable $th) {
            return new ServiceResponse(false, 'Erro ao atualizar bloqueio de contato', null);
        }
        return new ServiceResponse(true, 'Bloqueio de contato atualizado com sucesso', $contact);
    }

    /**
     * Updates a contact
     * @param int $userId
     * @param int $id
     *
     * @return ServiceResponse
     */
    public function update(array $inputs, int $id): ServiceResponse
    {
        DB::beginTransaction();
        try {
            $userId = $inputs['userId'];
            $response = $this->find($userId, $id);
            if (!$response->success) {
                DB::rollBack();
                return $response;
            }
            $contact = $this->contactRepository->update($inputs, $id);
            $contact->categories()->sync($inputs['category']);
            foreach ($inputs['phone'] as $idPhone => $phone) {
                app(PhoneRepository::class)->updateOrCreate(
                    ['id' => $idPhone,
                    'user_id' => $userId,
                    'contact_id' => $contact->id],
                    ['phone' => $phone]
                );
            }
            if (isset($inputs['deletePhones'])) {
                $phoneToDelete = explode(',', $inputs['deletePhones']);
                foreach ($phoneToDelete as $id) {
                    $this->phoneService->delete($userId, $id);
                }
            }
            foreach ($inputs['addresses'] as $addressId => $address) {
                if ($this->addressService->find($contact->id, $addressId)->success) {
                    app(AddressRepository::class)->update($address, $addressId);
                }
            }
            if (isset($inputs['newAddresses'])) {
                foreach ($inputs['newAddresses'] as $address) {
                    $addressParams = new CreateAddressServiceParams(
                        $address['cep'],
                        $address['logradouro'],
                        $address['complemento'],
                        $address['bairro'],
                        $address['localidade'],
                        $address['uf'],
                        $address['unidade'],
                        $address['ibge'],
                        $contact->id
                    );
                    $this->addressService->create($addressParams->toArray());
                }
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            return new ServiceResponse(false, 'Erro ao atualizar contato!', null);
        }
        DB::commit();
        return new ServiceResponse(true, 'Contato atualizado com sucesso!', $contact);
    }

    /**
     * Creates a new contact
     * @param array $params
     *
     * @return ServiceResponse
     */
    public function create(array $params): ServiceResponse
    {
        DB::beginTransaction();
        try {
            $contact = $this->contactRepository->create($params);
            foreach ($params['phones'] as $phone) {
                $phoneParams = new CreatePhoneServiceParams(
                    $phone,
                    $contact->id,
                    $params['users_id']
                );
                $this->phoneService->create($phoneParams->toArray());
            }
            $addresses = $params['addresses'];
            foreach ($addresses as $address) {
                $addressParams = new CreateAddressServiceParams(
                    $address['cep'],
                    $address['logradouro'],
                    $address['complemento'],
                    $address['bairro'],
                    $address['localidade'],
                    $address['uf'],
                    $address['unidade'],
                    $address['ibge'],
                    $contact->id
                );
                $this->addressService->create($addressParams->toArray());
            };
            foreach ($params['categories'] as $category) {
                $contact->categories()->attach($category);
            }
            event(new NewContactRegisteredEvent($contact));
        } catch (Exception $e) {
            DB::rollback();
            return new ServiceResponse(false, 'Erro ao criar contato!', null);
        }
        DB::commit();
        return new ServiceResponse(true, 'Contato criado com sucesso!', $contact);
    }
}
