<?php

namespace App\Services\Params;

use App\Services\Params\BaseServiceParams;

class CreateAddressServiceParams extends BaseServiceParams
{
    public $cep;
    public $logradouro;
    public $complemento;
    public $bairro;
    public $localidade;
    public $uf;
    public $unidade;
    public $ibge;
    public $contact_id;

    public function __construct(
        string $cep,
        string $logradouro,
        string $complemento,
        string $bairro,
        string $localidade,
        string $uf,
        string $unidade = null,
        string $ibge = null,
        int $contact_id
    ) {
        parent::__construct();
    }
}
