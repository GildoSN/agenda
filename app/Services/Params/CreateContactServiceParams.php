<?php

namespace App\Services\Params;

use Carbon\Carbon;
use App\Services\Params\BaseServiceParams;

class CreateContactServiceParams extends BaseServiceParams
{
    public $name;
    public $cpf;
    public $rg;
    public $email;
    public $status;
    public $users_id;
    public $phones;
    public $addresses;
    public $categories;

    public function __construct(
        string $name,
        string $cpf = null,
        string $rg = null,
        string $email,
        int $status,
        int $users_id,
        array $phones,
        array $addresses,
        array $categories
    ) {
        parent::__construct();
    }
}
