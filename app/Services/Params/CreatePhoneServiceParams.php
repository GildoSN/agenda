<?php

namespace App\Services\Params;

use Carbon\Carbon;
use App\Services\Params\BaseServiceParams;

class CreatePhoneServiceParams extends BaseServiceParams
{
    public $phone;
    public $contact_id;
    public $user_id;

    public function __construct(
        string $phone,
        int $contact_id,
        int $user_id
    ) {
        parent::__construct();
    }
}
