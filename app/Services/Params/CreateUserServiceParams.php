<?php

namespace App\Services\Params;

use App\Services\Params\BaseServiceParams;

class CreateUserServiceParams extends BaseServiceParams
{
    public $user_name;
    public $email;
    public $password;

    public function __construct(
        string $user_name,
        string $email,
        string $password
    ) {
        parent::__construct();
    }
}
