<?php

namespace App\Services;

use Exception;
use App\Repositories\LoginTokenRepository;
use App\Services\Responses\ServiceResponse;
use App\Services\Contracts\LoginTokenServiceInterface;

class LoginTokenService implements LoginTokenServiceInterface
{
    /**
     * @var LoginTokenRepository
     */
    private $loginTokenRepository;

    /**
     * @param LoginTokenRepository $categoryRepository
     */
    public function __construct(LoginTokenRepository $loginTokenRepository)
    {
        $this->loginTokenRepository = $loginTokenRepository;
    }

    /**
     * Returns contact by id
     * @param int $userId
     * @param string $loginToken
     *
     * @return ServiceResponse
     */
    public function delete(int $userId, string $loginToken): ServiceResponse
    {
        DB::beginTransaction();
        try {
            $loginToken = $this->loginTokenRepository->deleteWhere(['user_id' => $userId,'token' => $loginToken]);
            if ($loginToken == 0) {
                DB::rollback();
                return new ServiceResponse(false, 'loginToken inválido', null);
            }
        } catch (Exception $e) {
            DB::rollback();
            return new ServiceResponse(true, 'Erro ao deletar Token', null);
        }
        DB::commit();
        return new ServiceResponse(true, 'loginToken removido com sucesso', null);
    }
}
