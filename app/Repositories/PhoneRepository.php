<?php

namespace App\Repositories;

use App\Models\Phone;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface CategoryRepositoryRepository.
 *
 * @package namespace App\Repositories;
 */
class PhoneRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Phone::class;
    }
}
