<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ContactRepositoryRepository;
use App\Models\ContactRepository;
use App\Validators\ContactRepositoryValidator;

/**
 * Class ContactRepositoryRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ContactRepositoryRepositoryEloquent extends BaseRepository implements ContactRepositoryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ContactRepository::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
