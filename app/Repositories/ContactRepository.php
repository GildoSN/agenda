<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\Contact;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface ContactRepositoryRepository.
 *
 * @package namespace App\Repositories;
 */
class ContactRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Contact::class;
    }

    /**
     * return all contacts according to status.
     *
     * @param User $user
     * @param int $status
     */
    public function getContactByStatus(User $user, int $status)
    {
        return $user->contact()->where('status', '=', $status);
    }

    /**
     * searchs contact by name.
     *
     * @param User $user
     */
    public function getContactByName(User $user, string $name)
    {
        return $user->contact()->where('name', 'like', '%' . $name . '%');
    }
}
