<?php

namespace App\Repositories;

use App\Models\Address;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface CategoryRepositoryRepository.
 *
 * @package namespace App\Repositories;
 */
class AddressRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Address::class;
    }
}
