<?php

namespace App\Mail;

use stdClass;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmEmailMail extends Mailable implements ShouldQueue
{
    use Queueable;
    use SerializesModels;

    public $confirmUrl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(string $confirmUrl)
    {
        $this->confirmUrl = $confirmUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.confirmEmail');
    }
}
