<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class LoginToken extends Model
{
    protected $fillable = [
        'user_id','token'
    ];
    public static function generateFor(User $user)
    {
        return static::create([
            'user_id' => $user->id,
            'token' => Str::random(50),
        ]);
    }

    public function getRouteKeyName()
    {
        return 'token';
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
