<?php

namespace App\Models;

use App\Models\Contact;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'cep','logradouro','complemento','bairro','localidade','uf','unidade','ibge','contact_id'
    ];

    public function contacts()
    {
        return $this->belongsTo(Contact::class, 'contact_id')->withTimestamps();
    }
}
