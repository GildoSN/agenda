<?php

namespace App\Models;

use App\Models\Contact;
use App\Models\Category;
use Illuminate\Database\Eloquent\Model;

class CategoryContact extends Model
{
    protected $table = 'category_contact';

    protected $fillable = [
        'contact_id','category_id',
    ];


    public function contactCategory()
    {
        return $this->hasMany(Contact::class, 'contact_id')->withTimestamps();
    }

    public function categoryContact()
    {
        return $this->hasMany(Category::class, 'category_id')->withTimestamps();
    }
}
