<?php

namespace App\Models;

use App\Models\Contact;
use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{

    protected $fillable = [
        'phone','contact_id','user_id'
    ];

    public function phone()
    {
        return $this->belongsTo(Contact::class, 'contact_id')->withTimestamps();
    }
}
