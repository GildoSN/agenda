<?php

namespace App\Models;

use App\Models\User;
use App\Models\Phone;
use App\Models\Category;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';

    protected $fillable = [
        'name','cpf','rg','email','status','users_id','status','favorite'
    ];
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_contact', 'contact_id', 'category_id')->withTimestamps();
    }
    public function phones()
    {
        return $this->hasMany(Phone::class, 'contact_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'users_id');
    }
    public function addresses()
    {
        return $this->hasMany(Address::class, 'contact_id');
    }
}
