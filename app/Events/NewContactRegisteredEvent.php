<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class NewContactRegisteredEvent implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    public $contact;


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($contact)
    {
        $this->contact = $contact;
    }
}
