<?php

namespace App\Listeners;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\NewContactRegisteredEvent;
use App\Notifications\NewContactRegistered;
use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyViaSlack implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;
   


    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewContactRegisteredEvent  $event
     * @return void
     */
    public function handle(NewContactRegisteredEvent $event)
    {
        try{
            $user = User::where('id','=',$event->contact->users_id)->first();
            // dd($user);
            $user->notify(new NewContactRegistered());
        }
        catch(Exception $e){
            return $this->release(60);
        }
       
    }

    public function failed(NewContactRegisteredEvent $event, Exception $exception)
    {
       logger($exception);
    }
}
