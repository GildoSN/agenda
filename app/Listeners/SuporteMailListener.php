<?php

namespace App\Listeners;

use App\Mail\SuporteMail;
use App\Events\SuporteEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SuporteMailListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  SuporteEvent  $event
     * @return void
     */
    public function handle(SuporteEvent $event)
    {
        Mail::to($event->params['email'])->send(new SuporteMail($event->params));


        // Mail::send('mail.emailbody', ['request' => $event->inputs], function ($m) {
        //     $m->from($event->inputs['email'], 'teste');
        //     $m->to('customer@gmail.com', 'customer');
        // });
    }
}
