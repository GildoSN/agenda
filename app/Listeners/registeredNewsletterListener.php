<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;


class registeredNewsletterListener implements ShouldQueue
{
    /**'
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        logger('Registered to newsletter');
    }
}
