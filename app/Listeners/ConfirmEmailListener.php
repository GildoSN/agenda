<?php

namespace App\Listeners;

use App\Mail\ConfirmEmailMail;
use App\Events\ConfirmEmailEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmEmailListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  ConfirmEmailEvent  $event
     * @return void
     */
    public function handle(ConfirmEmailEvent $event)
    {
        Mail::to($event->params['email'])->send(new ConfirmEmailMail($event->params['url']));
    }
}
