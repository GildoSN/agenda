<?php

namespace App\Http\Requests;

use App\Rules\ValidaCpf;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Traits\SanitizesInput;

class NewContactRequest extends Request
{
    use SanitizesInput;

//     /**
//  *  Filters to be applied to the input.
//  *
//  *  @return array
//  */
// public function filters()
// {
//     return [
//         'unidade' => 'trim_null',
//         'ibge' => 'trim_null',
//     ];
// }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category'  => 'required',
            'phone.*'   => ['required','distinct', 'unique:phones,phone,' . $this->route('contact') . ',contact_id,user_id,' . Auth::id()],
            'cpf'       => ['nullable','unique:contacts', new ValidaCpf],
            'email'     => 'unique:contacts,email,NULL,id,users_id,' . Auth::id(),
            'addresses' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'category.required' => 'você deve inserir no mínimo 1 categoria para criar um contato',
            'cpf.unique' => 'Você ja tem um contato com esse cpf.',
            'email.unique' => 'Voce ja tem um contato com esse email.',
            'addresses.required' => 'O contato deve ter no minimo um endereço',
            'phone.*.required' => 'Os campos de telefone devem ser preenchidos ou excluidos',
            'phone.*.distinct' => 'O contato não pode ter 2 telefones repetidos',
            'phone.*.unique' => 'Outro contato possui esse telefone'
        ];
    }
}
