<?php

namespace App\Http\Requests;


use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class registrationvalidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'passwordconfirm' => 'same:password',
            'user_name'=>'required|alpha',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:6',
        ];
    }

    public function messages() 
    {
        return [
            'passwordconfirm.same' => 'A senha precisa ser igual',
            'password.min' =>'A senha deve conter no mínimo 6 caracteres',
            
            'user_name.alpha'=>'O nome não deve conter números'
        ];
    }
}
