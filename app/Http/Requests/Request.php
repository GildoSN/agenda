<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

abstract class Request extends FormRequest
{
    /**
     * Get the URL to redirect to on a validation error.
     *
     * @return string
     */
    protected function getRedirectUrl()
    {
        if ($this->has('hash') || $this->has('_hash')) {
            return parent::getRedirectUrl() .'#'. ($this->hash ?: $this->_hash);
        }

        return parent::getRedirectUrl();
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Http\Exceptions\HttpResponseException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = $validator->errors()->messages();

        if (request()->ajax() || request()->wantsJson()) {
            $response = response()->json($errors, 422);

            throw new HttpResponseException($response);
        }

        if (array_key_exists('g-recaptcha-response', $errors)) {
            $response = $this->redirector->to($this->getRedirectUrl())
                ->withInput()
                ->withErrors($errors)
                ->with('flashMsg', trans('validation.grecaptcha'))
                ->with('flashClass', 'danger');

            throw new HttpResponseException($response);
        }

        return parent::failedValidation($validator);
    }

    /**
     * Handle not authorized requests
     *
     * @throws \Illuminate\Http\Exceptions\HttpResponseException
     */
    protected function failedAuthorization()
    {
        $response = $this->redirector->to($this->getRedirectUrl())
            ->withInput()
            ->with('flashMsg', trans('validation.not_authorized'))
            ->with('flashClass', 'danger');

        throw new HttpResponseException($response);
    }
}
