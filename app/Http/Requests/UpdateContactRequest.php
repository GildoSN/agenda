<?php

namespace App\Http\Requests;

use App\Rules\ValidaCpf;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;
use App\Rules\ValidContactEmailUpdate;

class UpdateContactRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //
        // dd(request('phone'));
        return [
            'category'  => 'required',
            'cpf'       => ['nullable','unique:contacts,cpf, ' . $this->route('contact'), new ValidaCpf()],
            'email'     => new  ValidContactEmailUpdate(),
            'phone.*'   => ['required','distinct', 'unique:phones,phone,' . $this->route('contact') . ',contact_id,user_id,' . Auth::id()],
            'addresses' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'category.required' => 'você deve inserir no mínimo 1 categoria para editar um contato',
            'cpf.unique' => 'Você ja tem um contato com esse cpf',
            'phone.*.distinct' => 'O contato não pode ter 2 telefones repetidos',
            'phone.*.required' => 'Os campos de telefone devem ser preenchidos ou excluidos',
            'addresses.required' => 'O contato deve ter no minimo um endereço',
            'phone.*.unique' => 'Outro contato possui esse telefone'
        ];
    }
}
