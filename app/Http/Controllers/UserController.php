<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Exports\UsersExport;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\registrationvalidation;
use App\Services\Params\CreateUserServiceParams;

class UserController extends Controller
{
    /** @var UserService */
    private $userService;

    /**
     * Inicializa as variaveis do controller
     *
     * @param UserService $userService
     */
    public function __construct(
        UserService $userService
    ) {
        $this->userService = $userService;
    }

    /**
     * View registrar um usuário
     *
     * @return Response
     */
    public function register()
    {
        return view('register');
    }

    /**
     * Atualiza um usuário
     *
     * @param Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $userId = Auth::id();
        $inputs = $request->only([
            'user_name',
            'email',
        ]);
        $response = $this->userService->update($inputs, $userId);
        $this->flashErrorMessage($response);
        return redirect()->back()    ;
    }

    /**
     * Cria um novo usuário
     *
     * @param registrationvalidation $request
     * @return Response
     */
    public function store(registrationvalidation $request)
    {
        $params = new CreateUserServiceParams(
            $request->user_name,
            $request->email,
            bcrypt($request->password),
        );
        $response = $this->userService->create($params->toarray());
        $this->flashErrorMessage($response);
        return redirect()->back()->withmessage('Cheque seu e-mail');
    }
}
