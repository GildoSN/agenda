<?php

namespace App\Http\Controllers;

use DB;
use Excel;
use App\Models\Phone;
use App\Models\Contact;
use Illuminate\Http\Request;
use App\Services\ContactService;
use App\Services\ExportExcelService;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ExportExcelRequest;

class ExportExcelController extends Controller
{
    public $exportExcelService;

    public $contactService;

    /**
     * Class constructor.
     */
    public function __construct(ContactService $contactService, ExportExcelService $exportExcelService)
    {
        $this->contactService = $contactService;
        $this->exportExcelService = $exportExcelService;
    }

    public function search(Request $request)
    {
        $search = $request->get('search');
        $response = $this->contactService->searchContactByName(Auth::user(), $search);
        $contacts = $response->data->orderBy('favorite', 'desc')->paginate(30);
        $this->flashErrorMessage($response);
        return view('contactsExcel', ['contacts' => $contacts]);
    }

    public function excel(ExportExcelRequest $request)
    {
        $response = $this->contactService->findMultipleContacts(Auth::id(), $request->contactid);
        if (!$response->success) {
            return $this->redirectWithErrorFlash($response);
        }
        $response = $this->exportExcelService->contactsToExcel($response->data);
        if (!$response->success) {
            return $this->redirectWithErrorFlash($response);
        }
        $response->data->download('xlsx');
    }
}
