<?php

namespace App\Http\Controllers\api;

use App\Models\User;
use App\Models\Phone;
use App\Models\Contact;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\CategoryContact;
use App\Services\AddressService;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\newContactValidator;
use App\Http\Controllers\AddressController;
use App\Notifications\NewContactRegistered;
use App\Http\Controllers\CategoryController;
use App\Http\Requests\updateContactValidator;

class ApiContactController extends Controller
{
    public function index(Request $request)
    {
        $user_id = Auth::user()->id;
        $contacts = Contact::where([
            ['status','=',1],
            ['users_id','=',$user_id]])->paginate(15);
        return response()->json(['message'=>"esses são seus contatos:",'contacts' => $contacts]); //retorna um json com todos os contatos
    }

    public function create()
    {
    }

    public function store(newContactValidator $request){

        // dd(Auth::user()->id);

        $contact = new Contact;
        // dd($contact);

        $contact->name = $request->input('name');
        // dd($request->name);
        $contact->cpf = request('cpf');
        $contact->rg = request('rg');
        $contact->email = request('email');
        $contact->status = 1;

        $contact->users_id = Auth::user()->id;

        $contact->save();


        // dd($request->phone);
        foreach($request->phone as $phone)
        {
            $phones = new Phone([
                'phone'     =>$phone,
                'contact_id'=>$contact->id
            ]);

            $phones->save();
        }
        $contact->phones;



        $category = Category::where('user_id', '=', $contact->users_id)
        ->pluck('category', 'id')
        ->toArray();

        // dd($category);
        // if($request->category)
        foreach($request->category as $request_category)
        {
            if(in_array($request_category,$category)){

                $category_find = Category::where([
                    ['user_id', '=', $contact->users_id],
                    ['category','=',$request_category],
                    ])->first();

                // ->pluck('id')
                // ->toArray();

                // dd($category_find->id);
                // dd($category_find);

                $categorie = new CategoryContact([
                'contact_id'=>$contact->id,
                'category_id'=>$category_find->id,
                ]);

                $categorie->save();
                continue;
                // dd($category);
            }
            // dd($request_category);

            app(CategoryController::class)->storeApi($request,$contact->id,$contact->users_id,$request_category);
        }
        $contact->categories;

        app(AddressController::class)->store($request,$contact->id);
        $contact->addresses;

        // dd($contact);

        // dd($contact->phones);
        // dd($phones);
        if($contact->save()){
            Auth::user()->notify(new NewContactRegistered());
            return response()->json(['contacts' => $contact]);
        }
    }

    public function destroy(Request $request,$id)
    {
        // dd(Auth::user()->id);
        // $user_id = $users->id;
        $contact = Contact::where([['users_id',Auth::user()->id],['id',$id]])->first();
        // dd($contact);

        if(is_null($contact)){
            return 'Unauthorized.';
        }
        else{
            // dd("asda");
            $contact->delete();
            return response()->json(['contato deletado' => $contact]);;
            // return response()->json(['contact_deleted' => $contact]); //returns a json with deleted_contact
        }
    }
    public function update(updateContactValidator $request,$id)
    {
        $contact = Contact::Where([['id',$id],['users_id',Auth::user()->id]])->first();
        // dd($contact);
        if(is_null($contact)){
            return 'Unauthorized';
        }
        else if(@empty($contact)){
            return 'Unauthorized';
        }
        else{
            $contact->name = $request->input('name');
            // dd($request->name);

            if(is_null($request->cpf)){
                $contact->cpf = "";
            }
            else{
            $contact->cpf = request('cpf');
            }
            $contact->rg = request('rg');
            $contact->email = request('email');
            $contact->status = 1;

            $contact->users_id = Auth::user()->id;

            $contact->save();

            $phoneses = $contact->phones
            ->pluck('phone','id')
            ->toArray();


            // dd($contact->categories);

            $categories = $contact->categories //a variavel categories = categorias já escolhidas
            ->pluck('id')
            ->toArray();

            // dd($categories);

            // dd($request->category);
            if($request->category!=null)
            {
                $d=$request->category;
                //  dd($d);  //= marcados
                //dd($categories); //= todas as categorias registradas com esse contato

                $diferencaa=array_diff($categories,$d);

                //  dd($diferencaa);

                foreach($diferencaa as $key=>$dif_dos_submits)
                {
                    // dd($dif_dos_submits);

                    DB::table('category_contact')->where([
                        ['category_id', '=', $dif_dos_submits],
                        ['contact_id', '=', $contact->id],
                    ])->delete();
                }
            }
            // dd($contact->categories);
            $category = Category::where('user_id', '=', $contact->users_id)
            ->pluck('category', 'id')
            ->toArray();

            // dd($category);
            // dd($request->category);

            foreach($request->category as $category)
            {
                $categorie = Category::where([['category',$category],['user_id',Auth::user()->id]])->first();
                // dd($contact->id);
                // dd(DB::table('category_contact')->where('category_id', '=', $category)->get());
                // dd(CategoryContact::where([['category_id',$category],['contact_id',$contact->id]])->get());
                // dd($categorie->id);
                if (CategoryContact::where([
                    ['category_id',$categorie->id],
                    ['contact_id',$contact->id]
                    ])->get()->isNotEmpty()) {
                }
                else{
                $category = new CategoryContact([
                    'contact_id'=>$contact->id,
                    'category_id'=>$categorie->id
                ]);
                $category->save();
                }
            }

            app(AddressService::class)->update($request,$contact->id);
            $contact->addresses;
            // dd($contact);
            if($request->newphone !=null){
                foreach($request->newphone as $newphone)
                {
                    if($newphone!=null)
                    {

                        $id_user = Auth::user()->id;
                        // dd($id_user);
                        $phones = new Phone([
                            'phone'     =>$newphone,
                            'contact_id'=>$contact->id,
                        ]);
                        $phones->user_id = $id_user;

                        $user_phones = Phone::where([
                            ['phone','=',$newphone],
                            ['user_id','=',$id_user],
                        ])->get();

                        $phones->save();
                    }
                }
            }


            if(!is_null($request->phone)){
                foreach($request->phone as $key => $phone)
                {
                    $phones = Phone::find($key);
                    $phones->phone=$phone;
                    $phones->contact_id=$contact->id;

                    $phones->save();
                }
                // dd($request);

                $request_phone=$request->phone;

                $diferenca = array_diff_key($phoneses,$request_phone);

                // dd($diferenca,$request_phone,$phoneses);

                foreach($diferenca as $key=>$dif){
                    DB::table('phones')->where('id',$key)->delete();
                }
            }



            // dd($contact->phones);
            // dd($phones);
            if($contact->save()){
                Auth::user()->notify(new NewContactRegistered());
                return response()->json(['contacts' => $contact]);
            }
        }
    }
}
