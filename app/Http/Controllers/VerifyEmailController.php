<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\LoginToken;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Events\ConfirmEmailEvent;
use App\Services\LoginTokenService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class VerifyEmailController extends Controller
{
    /** @var UserService */
    private $userService;

    /** @var LoginTokenService */
    private $loginTokenService;

    /**
     * Inicializa as variaveis do controller
     *
     * @param UserService $userService
     * @param LoginTokenService $loginTokenService
     */
    public function __construct(
        UserService $userService,
        LoginTokenService $loginTokenService
    ) {
        $this->userService = $userService;
        $this->loginTokenService = $loginTokenService;
    }

    public function index()
    {
        return view('verifyEmail');
    }

    /**
     * Verifica o email e seta verified_at
     *
     * @param LoginToken $token
     * @return Response
     */
    public function verifyEmail(LoginToken $token)
    {
        $params = [
            'email_verified_at' => Carbon::now()->toDateTimeString(),
        ];
        $response = $this->userService->update($params, $token->user_id);
        if (!$response->success) {
            return $this->redirectWithErrorFlash($response);
        }
        $this->loginTokenService->delete($token->user_id, $token->token);
        return redirect('/')->withSuccess('Email Verificado!');
    }

    public function sendNewVerifyEmail(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if (is_null($user)) {
            return redirect()->back()->withErrors('Email incorreto ou inexistente!');
        }
        if (!is_null($user->email_verified_at)) {
            return redirect()->back()->withErrors('Email ja foi Verificado');
        }

        $token = LoginToken::generateFor($user);
        $confirmEmailparams = [
            'email' => $request['email'],
            'url' => url('login/token', $token),
        ];
        event(new ConfirmEmailEvent($confirmEmailparams));
        return redirect()->back()->withmessage('Cheque seu e-mail');
    }
}
