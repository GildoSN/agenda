<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\AuthenticatesUser;
use App\Mail\ConfirmEmail;
use App\Models\LoginToken;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Events\ConfirmEmailEvent;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\loginvalidator;
use App\Http\Requests\registrationvalidation;

class AuthenticationController extends Controller
{
    protected $auth;

    public function __construct(AuthenticatesUser $auth)
    {
        $this->auth = $auth;
    }

    public function login()
    {
        return view('login');
    }

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function authenticate(loginvalidator $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            if (is_null(Auth::user()->email_verified_at)) {
                return redirect()->back()->withMessage("E-mail não verificado");
            }
            // Authentication passed...
            return redirect()->intended('dashboard');
        }
        session()->flash('error', 'email ou senha incorretos');
        return back();
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
