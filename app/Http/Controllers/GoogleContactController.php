<?php

namespace App\Http\Controllers;

use App\Models\Phone;
use App\Models\Contact;
use Artdarek\OAuth\OAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GoogleContactController extends Controller
{
    public function importGoogleContact(Request $request)
    {
        // get data from request
        $code = $request->get('code');
        // dd($code);
        // get google service
        $googleService = \OAuth::consumer('Google');
        // check if code is valid

        // if code is provided get user data and sign in
        if ( ! is_null($code)) {
            try {

                $token = $googleService->requestAccessToken($code);

              } catch (\Exception $e) {
                // dd($e)
                //   return redirect("https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://127.0.0.1:8000/contact/import/google") ;
                  return redirect("http://127.0.0.1:8000/contact/import/google") ;
                //   return redirect("https://www.google.com/accounts/continue") ;
              }
            // This was a callback request from google, get the token

            // dd($token);
            // dd($token);
            // dd(json_decode($googleService->request('https://www.google.com/m8/feeds/contacts/default/full?alt=json&amp;max-results=200'), false));
            // Send a request with it
            $result = json_decode($googleService->request('https://www.google.com/m8/feeds/contacts/default/full?alt=json&max-results=10000'), true);

            // dd($result);

            // Going through the array to clear it and create a new clean array with only the email addresses
            $emails = []; // initialize the new array
            $name = [];
            $phoneNumber = [];

            foreach ($result['feed']['entry'] as $contact) {
                if (isset($contact['gd$email'])) { // Sometimes, a contact doesn't have email address
                    $emails[] = $contact['gd$email'][0]['address'];
                }
                else{
                    $emails[]=null;
                }
                // dd($contact['title']);
                if (!empty($contact['title']['$t'])) {
                    $name[] = ($contact['title']['$t']); //$name = all contacts names
                }
                else{


                    $name[]="insira um nome";
                }
                if (isset($contact['gd$phoneNumber'])) {
                $phoneNumber[] = ($contact['gd$phoneNumber'][0]['$t']);
                }
                else{
                    $phoneNumber[]=''; //$phoneNumber = all contacts phone numbers
                }

                // $phoneNumber[]=$contact['gd$phoneNumber'];

            }
            // dd($contact['title']);
            // dd($name);
            $objname = (object) $name;

            $numberCorrect=[];
            $ee=55;
            $testtt=[];
            foreach($phoneNumber as $id=>$phone){
                // dd($phone);
                $numberCorrect[] =  preg_replace('/[^0-9]/', '',$phone);
                // $numberCorrect[] = str_replace(55,'',$phone);
                // $numberCorrect[] = preg_replace('/[^0-9]/', '',$phone);
                // $remove55 = preg_replace('@^'.$ee.'@','',$phone);
                // dd($numberCorrect);
                // $testtt[] = $remove55;
                // dd($remove55);
            }
            // dd($numberCorrect);
            // dd($correto);
            // dd($name,$numberCorrect,$emails);
            // dd($objname);
            $contact=[];
            for($i=0;$i<count($name);$i++){
                $contact[]=[
                'name' => $name[$i],
                'phone'=>$numberCorrect[$i],
                'email'=>$emails[$i]
                ];
            }
            // dd($contact);
            // dd($name);


            // $contact->paginate(10);
            // dd($contact[0]['name']);
            return view('addGoogleContact',compact('contact'));
        }

        // if not ask for permission first
        else {
            // get googleService authorization
            $url = $googleService->getAuthorizationUri();

            // return to google login url
            return redirect((string)$url);
        }
    }

    function store(Request $request){
        $i=0;
            $contatos_repetidos = [];
            if(is_null($request->name)){
                return redirect()->route('contacts.index')->withErrors("selecione um contato para importar contatos do Google");
            }
            foreach($request->name as $key=>$name){
                // dd($request->phone);
                $phone = preg_replace( '/[^0-9]/is', '', $request->phone[$i] );
                $contact = new Contact();
                $contact->name = $name;

                $contact->cpf = null;
                // dd('aoao');

                $contact->rg = null;

                $contact->email = $request->email[$i];

                $contact->favorite=0;
                $contact->status = 1;

                $contact->users_id = Auth::user()->id;
                $contact->save();
                // dd($contact);

                $id_user = Auth::user()->id;

                $phones = new Phone([
                    'phone'     =>$phone,
                    'contact_id'=>$contact->id,
                ]);


                $phones->user_id = $id_user;
                $user_phones = Phone::where([
                    ['phone','=',$phone],
                    ['user_id','=',$id_user],
                ])->get();
                    // dd($user_phones);


                // if($i == 3){
                //     dd($user_phones);
                // }
                if($user_phones->isEmpty()){
                    $phones->save();
                }
                else{
                    $contatos_repetidos[] = $contact->name;
                    Contact::where('id',$contact->id)->delete();
                }

                $i=$i+1;
            }
            // dd($phoneNumber);
            // dd($name,$numberCorrect);
            // dd($contatos_repetidos);
            if(empty($contatos_repetidos)){
                return redirect('/contacts');
            }
            else{
                return redirect('/contacts')->withErrors(['os seguintes contatos não foram salvos pois estavam repetidos',$contatos_repetidos]);
            }
            // return $emails;

    }

}
