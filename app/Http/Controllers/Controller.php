<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    /**
     * Flashs error and success messages
     *
     * @param collection $response
     * @return response
     */
    protected function flashMessage($response)
    {
        return $response->success ? flash($response->message)->success() : flash($response->message)->error();
    }

    /**
     * Flashs only error messages
     *
     * @param collection $response
     * @return response
     */
    protected function flashErrorMessage($response)
    {
        return !$response->success ? flash($response->message)->error() : null;
    }

    /**
     * Creates Multiple error Messages
     *
     * @param array $responses
     * @return Response
     */
    protected function flashMultipleErrorMessage(array $responses)
    {
        foreach ($responses as $response) {
            $this->flashErrorMessage($response);
        }
    }

    public function redirectWithErrorFlash($response)
    {
        $this->flashErrorMessage($response);
        return redirect()->back();
    }

    /**
     * Creates Multiple error Messages
     *
     * @param array $responses
     * @return Response
     */
    protected function redirectWithMultipleErrorFlash(array $responses)
    {
        foreach ($responses as $response) {
            $this->flashErrorMessage($response);
        }
        return redirect()->back();
    }
}
