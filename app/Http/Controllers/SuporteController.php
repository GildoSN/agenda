<?php

namespace App\Http\Controllers;

use App\Events\SuporteEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SuporteController extends Controller
{
    /**
     * View de suporte
     *
     * @return Response
     */
    public function index()
    {
        return view('suporte.suporteview');
    }

    /**
     * cria um evento de suporte
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $params = $request->only([
            'email',
            'assunto',
            'texto',
        ]);
        event(new SuporteEvent($params));
        return redirect()->back();
    }
}
