<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Services\PhoneService;
use App\Services\ContactService;
use App\Services\CategoryService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\NewContactRequest;
use App\Http\Requests\UpdateContactRequest;
use App\Services\Params\CreateContactServiceParams;

class ContactController extends Controller
{
    /** @var ContactService */
    private $contactService;

    /** @var PhoneService */
    private $phoneService;

    /** @var CategoryService */
    private $categoryService;

    /**
     * Inicializa as variaveis do controller
     *
     * @param ContactService    $contactService
     * @param CategoryService    $categoryService
     * @param PhoneService $phoneService
     */
    public function __construct(
        ContactService $contactService,
        CategoryService $categoryService,
        PhoneService $phoneService
    ) {
        $this->contactService = $contactService;
        $this->categoryService = $categoryService;
        $this->phoneService = $phoneService;
    }

    /**
     * View de contatos
     * GET contacts
     *
     * @return Response
     */
    public function index()
    {
        $userId = Auth::user();
        $response = $this->contactService->getContactByStatus($userId, 1);
        if (!$response->success) {
            return $this->redirectWithErrorFlash($response);
        }
        $contacts = $response->data->orderBy('favorite', 'desc')->paginate(10);
        return view('contacts', compact('contacts'));
    }
    /**
     * Favorita um contato
     * GET contacts/favorite/{id}
     *
     * @param int $idContact
     * @return Response
     */
    public function favorite(int $idContact)
    {
        $userId = Auth::id();
        $response = $this->contactService->favorite($userId, $idContact);
        $this->flashErrorMessage($response);
        return redirect()->back();
    }

    /**
     * View de criar um contato
     * GET contacts/create
     *
     * @return Response
     */
    public function create()
    {
        $response = $this->categoryService->all(Auth::id());
        if (!$response->success) {
            return $this->redirectWithErrorFlash($response);
        }
        $category = $response->data
        ->pluck('category', 'id')
        ->toArray();
        $this->flashErrorMessage($response);
        return view('newcontact', compact('category'));
    }

    public function search(Request $request)
    {
        $search = $request->get('search');
        $response = $this->contactService->searchContactByName(Auth::user(), $search);
        $contacts = $response->data->orderBy('favorite', 'desc')->paginate(10);
        $this->flashErrorMessage($response);
        return view('contacts', ['contacts' => $contacts]);
    }

    /**
     * Cria um contato
     * POST contacts
     *
     * @param NewContactRequest $request
     * @return Response
     */
    public function store(NewContactRequest $request)
    {
        $userId = Auth::id();
        $params = new CreateContactServiceParams(
            $request->name,
            $request->cpf,
            $request->rg,
            $request->email,
            1,
            $userId,
            $request->phone,
            $request->addresses,
            $request->category
        );
        $response = $this->contactService->create($params->toArray());
        DB::commit();
        $this->flashMessage($response);
        return redirect()->back()->withInput();
    }

    /**
     * View para editar um contato
     * GET contacts/{contact}/edit
     *
     * @param int $id
     * @return \illuminate\Http\Response
     */
    public function edit(int $id, $responses = [])
    {
        $userId = Auth::id();
        $responses['contact'] = $this->contactService->find($userId, $id);
        $responses['category'] =  $this->categoryService->all(Auth::id());
        if (!$responses['contact']->success || !$responses['category']->success) {
            return $this->redirectWithMultipleErrorFlash($responses);
        }
        $category = $responses['category']->data->pluck('category', 'id')->toArray();
        $contact = $responses['contact']->data;
        $responses['phones'] =  $this->phoneService->all($userId, $contact->id);
        $phones = $responses['phones']->data;
        $categories = $contact->categories->pluck('id')->toArray();
        $this->flashMultipleErrorMessage($responses);
        return view('edit', compact('category', 'contact', 'phones', 'categories'));
    }
    /**
     * Atualiza um contato
     * PUT contacts/{contact}
     *
     * @param int $id
     * @return \illuminate\Http\Response
     */
    public function update(UpdateContactRequest $request, $id)
    {
        $userId = Auth::id();
        $inputs = $request->only([
            'name',
            'cpf',
            'rg',
            'email',
            'category',
            'phone',
            'deletePhones',
            'addresses',
            'newAddresses'
        ]);
        $inputs['userId'] = $userId;
        $response = $this->contactService->update($inputs, $id);
        $this->flashMessage($response);
        return redirect()->back();
    }

    /**
     * Deleta um contato
     * DELETE contacts/{contact}
     *
     * @param int $id
     * @return Response
     */
    public function destroy(int $id)
    {
        $userId = Auth::id();
        $response = $this->contactService->delete($userId, $id);
        $this->flashMessage($response);
        return redirect()->back();
    }

    /**
     * Bloqueia um contato
     * PUT contacts/{contact}
     *
     * @param int $id
     * @return Response
     */
    public function block(int $id)
    {
        $userId = Auth::id();
        $response = $this->contactService->blockedUpdate($userId, $id, 0);
        $this->flashMessage($response);
        return redirect()->back();
    }

    /**
     * Desbloqueia um contato
     * PUT contacts/{contact}
     *
     * @param int $id
     * @return Response
     */
    public function unblock($id)
    {
        $userId = Auth::id();
        $response = $this->contactService->blockedUpdate($userId, $id, 1);
        $this->flashMessage($response);
        return redirect()->back();
    }

    /**
     * View de contatos bloqueados
     *
     * @return Response
     */
    public function show()
    {
        $user = Auth::user();
        $response =  $this->contactService->getContactByStatus($user, 0);
        $blockedContacts = $response->data->orderBy('favorite', 'desc')->paginate(10);
        $this->flashErrorMessage($response);
        return view('block', compact('blockedContacts'));
    }

    /**
     * View de exportação de contatos para excel
     *
     * @return Response
     */
    public function excelIndex()
    {
        $user = Auth::user();
        $response =  $this->contactService->getContactByStatus($user, 1);
        $contacts = $response->data->orderBy('favorite', 'desc')->paginate(20);
        return view('contactsExcel', compact('contacts'));
    }
}
