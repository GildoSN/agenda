<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\PhoneService;
use Illuminate\Support\Facades\Auth;

class PhoneController extends Controller
{
    /** @var PhoneService */
    private $phoneService;

    /**
     * Inicializa as variaveis do controller
     *
     * @param PhoneService $phoneService
     */
    public function __construct(PhoneService $phoneService) {
        $this->phoneService = $phoneService;
    }
    public function destroy(int $id)
    {
        $userId = Auth::id();
        $message = $this->phoneService->delete($userId, $id)->message;
        return $message;
    }
}
