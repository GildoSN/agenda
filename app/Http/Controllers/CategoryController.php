<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\CategoryContact;
use App\Services\CategoryService;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /** @var CategoryService */
    private $categoryService;

    /**
     * Inicializa as variaveis do controller
     *
     * @param CategoryService    $categoryService
     */
    public function __construct(
        CategoryService $categoryService
    ) {
        $this->categoryService = $categoryService;
    }

    /**
     * View de categorias
     * GET categories
     *
     * @return Response
     */
    public function index()
    {
        $userId = Auth::id();
        $response = $this->categoryService->all($userId);
        if (!$response->success) {
            return $this->redirectWithErrorFlash($response);
        }
        $category = $response->data->pluck('category', 'id')->toArray();
        return view('newcategory', compact('category'));
    }

    /**
     * Cria uma categoria
     * POST categories
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $params = $request->only([
            'category'
        ]);
        $params['user_id'] = Auth::id();
        $response = $this->categoryService->create($params);
        $this->flashMessage($response);
        return redirect()->back();
    }

    /**
     * Deleta uma categoria
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $response = $this->categoryService->delete(Auth::id(), $id);
        $this->flashMessage($response);
        return redirect()->back();
    }

    /**
     * Atualiza uma categoria
     *
     * @param Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $inputs = $request->only([
            'category'
        ]);
        $categoryId = $request->category_id;
        $response = $this->categoryService->update($inputs, $categoryId);
        $this->flashMessage($response);
        return redirect()->back();
    }

    // public function storeApi(Request $request, $id, $users_id, $request_category)
    // {
    //     $category = new Category();


    //     $category->category = $request_category;

    //     $category->user_id = $users_id;
    //     // dd($request->category);

    //     $category->save();
    //     // dd($category);
    //     $categorie = new CategoryContact([
    //         'contact_id'=>$id,
    //         'category_id'=>$category->id,
    //     ]);
    //     $categorie->save();
    // }
}
