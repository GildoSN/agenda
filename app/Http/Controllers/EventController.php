<?php

namespace App\Http\Controllers;

use Calendar;
use DateTime;
use App\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    public function index()
    {

        $events = [];
        //$data = Event::all();

        $data = Event::where('user_id', '=', Auth::user()->id)->get();

        if ($data->count()) {
            foreach ($data as $value) {
                $events[] = Calendar::event(
                    $value->title,
                    false,
                    new DateTime($value->start_date),
                    new DateTime($value->end_date . ' +1 day')
                );
                // dd($events);
            }
        }

        $calendar = Calendar::addEvents($events);
        return view('dashboard', compact('calendar'));
    }

    public function eventsView()
    {
        $events = Event::where('user_id', '=', Auth::user()->id)->get()->pluck('start_date', 'end_date')->toarray();
        // dd($events);
        foreach ($events as $end_date => $start_date) {
            $inicio = Carbon::parse($start_date)->format('d-m-Y H:i');
            $fim = Carbon::parse($end_date)->format('d-m-Y H:i');
            $event_inicio[] = $inicio;
            $event_fim[] = $fim;
        }
        $events = Event::where('user_id', '=', Auth::user()->id)->get();

        // dd($event_fim[11]);

        // $event = (object) $event_dates;


        // dd($events,$event_inicio,$t);
        // $inicio
        // dd($events);
        return view('events', compact('events', 'event_inicio', 'event_fim'));
    }

    public function store(Request $request)
    {
        // dd($request);

        $inicio = Carbon::parse($request->start_date);
        $fim = Carbon::parse($request->end_date);

        $inicio->format('Y M d');
        $fim->format('Y M d');
        // dd($fim);
        $title = preg_replace('/[^\w]/', '', $request->title);

        $event = new Event();
        $event->title = $title;
        $event->start_date = $inicio;
        $event->end_date = $fim;
        $event->user_id = Auth::user()->id;
        $event->save();
        return redirect()->back();
    }

    public function update(Request $request)
    {
        // dd($request);
        $inicio = Carbon::parse($request->start_date);
        // dd($inicio);

        $fim = Carbon::parse($request->end_date);

        $inicio->format('Y M d');
        $fim->format('Y M d');

        // dd($fim);
        $events = Event::find($request->event_id);

        $title = preg_replace('/[^\w]/', '', $request->event_title_edit);
        // dd($events);
        // dd($events);
        $events->title = $title;
        $events->start_date = $inicio;
        $events->end_date = $fim;
        $events->save();
        // dd($events);
        return redirect()->back();
    }


    public function destroy($id)
    {

        $events = Event::find($id);
        // dd($events);
        // dd($events);
        if (is_null($events)) {
            return redirect()->back();
        }
        $events->delete();

        return redirect()->back();
    }
}